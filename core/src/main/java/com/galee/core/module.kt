package com.galee.core

import com.karumi.dexter.Dexter
import org.koin.dsl.module

val coreModule = module {
    single { Dexter.withActivity(get()) }
}