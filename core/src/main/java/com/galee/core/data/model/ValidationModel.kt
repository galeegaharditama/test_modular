package com.galee.core.data.model

data class ValidationModel(var status:Boolean, var message:String?, var layout:Int){

    constructor(status: Boolean, message: String?) : this(status, message, -1)
    constructor() : this(false,null, -1)
}

data class Validation(var isValid:Boolean, var validationFields: MutableList<ValidationModel>)