package com.galee.core

import android.content.Context
import androidx.lifecycle.ViewModel
import com.galee.domain.domain.UserDomain
import timber.log.Timber

abstract class BaseVM(context: Context) : ViewModel() {
    protected var mContext: Context = context

    /*DON'T FORGET TO UPDATE THIS PROFILE*/
    val mProfil by lazy {
        val pref = context.getSharedPreferences("profil.conf", Context.MODE_PRIVATE)
        UserDomain(
            idUser = pref.getInt("id_user", -1),
            idJabatan = pref.getString("id_jabatan", "") ?: "",
            jabatanUser = pref.getString("jabatan_user", "") ?: "",
            namaUser = pref.getString("nama_user", "") ?: "",
            phone = pref.getString("phone", "") ?: "",
            username = pref.getString("username", "") ?: "",
            tokenType = pref.getString("token_type", "") ?: "",
            expiresIn = pref.getLong("expires_in", 0),
            accessToken = pref.getString("access_token", "") ?: "",
            refreshToken = pref.getString("refresh_token", "") ?: "",
            masaBerlaku = pref.getString("masa_berlaku", "") ?: ""
        )
    }

    abstract fun getTagName(): String

    override fun onCleared() {
        super.onCleared()
        Timber.i("${getTagName()} onCleared() called")
    }
}