package com.galee.core

import android.app.Activity
import android.app.Dialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.galee.core.util.hideSoftKeyboard
import com.galee.core.util.widget.SnackBarCustom
import com.galee.core.util.widget.ToastCustom
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import timber.log.Timber

abstract class BaseFragment : Fragment(), BaseFragmentImplDialog,
    BaseFragmentImplSnackBar, BaseFragmentImplToast, BaseFragmentImplPermission {

    abstract fun getTagName(): String
    @LayoutRes
    abstract fun layoutId(): Int
    abstract fun onCreateUI(view: View, savedInstanceState: Bundle?)

    private var pDialog: Dialog? = null
    lateinit var mView: View
    protected val bundle = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("${getTagName()} onCreate() called")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.i("${getTagName()} onCreateView() called")
        mView = inflater.inflate(layoutId(), container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideSoftKeyboard()
        Timber.i("${getTagName()} onViewCreated() called")
        onCreateUI(view, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.i("${getTagName()} onSaveInstanceState() called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("${getTagName()} onDestroy() called")
    }

    override fun showDialogLoading(message: String?) {
        if (pDialog == null) pDialog = Dialog(mView.context)

        pDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        pDialog?.setContentView(R.layout.dialog_loading)
        pDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        val progressBar = pDialog?.findViewById(R.id.progressBar) as ProgressBar
        val msg = pDialog?.findViewById(R.id.txt_message) as TextView
        if (message != null) msg.text = message
        else msg.text = getString(R.string.base_txt_loading)
        pDialog?.setCancelable(false)
        pDialog?.show()
    }

    override fun hideDialogLoading() {
        if (pDialog != null && pDialog?.isShowing!!) {
            pDialog?.dismiss(); pDialog = null
        }
    }

    override fun showToastMessage(message: String, duration: Int) {
        activity?.let {
            ToastCustom(it).get("", message, duration).show()
        } ?: throw Exception("Activity is Null")
    }

    /**
     * @param type => 'success', 'danger', 'info' or '' as String
     * @param message => message of Toast
     * @param duration => duration of Toast
     **/
    override fun showToastMessage(type: String, message: String, duration: Int) {
        activity?.let {
            ToastCustom(it).get(type, message, duration).show()
        } ?: throw Exception("Activity is Null")
    }

    override fun showSnackBarMessage(message: String, duration: Int): Snackbar? {
        activity?.let {
            val snackbar = SnackBarCustom(it).get("", message, duration)
            snackbar.show()
            return snackbar
        }
        return null
    }

    /**
     * @param type => 'success', 'danger', 'info' or '' as String
     * @param message => message of Snackbar
     * @param duration => duration of Snackbar
     **/
    override fun showSnackBarMessage(type: String, message: String, duration: Int): Snackbar? {
        activity?.let {
            val snackbar = SnackBarCustom(it).get(type, message, duration)
            snackbar.show()
            return snackbar
        }
        return null
    }

    protected fun isHasPermission(permissions: MutableList<String>): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            permissions.all { singlePermission ->
                mView.context.checkSelfPermission(singlePermission) == PackageManager.PERMISSION_GRANTED
            }
        else true
    }

    protected fun askPermission(vararg permissions: String, @androidx.annotation.IntRange(from = 0) requestCode: Int) =
        ActivityCompat.requestPermissions(activity as Activity, permissions, requestCode)

    protected fun onRunPermission(permissions: MutableList<String>, level: Int) {
        Timber.i("${getTagName()} onRunPermission() called")
        val view: View? = activity?.findViewById(android.R.id.content)
        Dexter.withActivity(activity)
            .withPermissions(permissions)
            .withListener(
                CompositeMultiplePermissionsListener(
                    SnackbarOnAnyDeniedMultiplePermissionsListener.Builder
                        .with(view, R.string.base_permission_title)
                        .withOpenSettingsButton(R.string.base_permission_btn_text)
                        .withDuration(Snackbar.LENGTH_LONG)
                        .build(),
                    DialogOnAnyDeniedMultiplePermissionsListener.Builder
                        .withContext(mView.context)
                        .withTitle(R.string.base_permission_title)
                        .withMessage(R.string.base_permission_message)
                        .withButtonText(android.R.string.ok)
//                        .withIcon(R.mipmap.ic_logo)
                        .build(),
                    object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                            report?.let {
                                if (it.areAllPermissionsGranted()) {
                                    onAllPermissionGranted(level)
                                } else {
                                    onDenyPermission(level)
                                }
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: MutableList<PermissionRequest>?,
                            token: PermissionToken?
                        ) {
                            token?.continuePermissionRequest()
                        }
                    }
                )
            ).onSameThread().check()
    }

    override fun onAllPermissionGranted(level: Int) {
        Timber.i("${getTagName()} onAllPermissionGranted() called")
    }

    override fun onDenyPermission(level: Int) {
        Timber.i("${getTagName()} onDenyPermission() called")
    }

}

interface BaseFragmentImplPermission {
    fun onAllPermissionGranted(level: Int)
    fun onDenyPermission(level: Int)
}

interface BaseFragmentImplDialog {
    fun showDialogLoading(message: String?)
    fun hideDialogLoading()
}

interface BaseFragmentImplToast {
    fun showToastMessage(message: String, duration: Int = Toast.LENGTH_LONG)
    fun showToastMessage(type: String, message: String, duration: Int = Toast.LENGTH_LONG)
}

interface BaseFragmentImplSnackBar {
    fun showSnackBarMessage(message: String, duration: Int = Snackbar.LENGTH_LONG): Snackbar?
    fun showSnackBarMessage(
        type: String,
        message: String,
        duration: Int = Snackbar.LENGTH_LONG
    ): Snackbar?
}