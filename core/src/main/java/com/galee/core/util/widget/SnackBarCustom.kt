package com.galee.core.util.widget

import android.app.Activity
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.galee.core.R
import com.galee.core.util.displaySnakbar
import com.google.android.material.snackbar.Snackbar

class SnackBarCustom(activity: Activity) {
    private var mActivity: Activity = activity

    fun get(
        type: String,
        message: String,
        duration: Int = Snackbar.LENGTH_LONG
    ): Snackbar {
        when (type) {
            "success" -> return showSnackbarSuccess(message, duration)
            "danger" -> return showSnackbarError(message, duration)
            "info" -> return showSnackbarInfo(message, duration)
        }
        return mActivity.findViewById<View>(android.R.id.content)
            .displaySnakbar(message, Snackbar.LENGTH_LONG)
    }

    private fun showSnackbarSuccess(message: String, duration:Int): Snackbar {
        val view: View = mActivity.findViewById(android.R.id.content)
        val snackbar = Snackbar.make(view, "", duration)
        val customView = mActivity.layoutInflater.inflate(R.layout.snackbar_icon_text, null)
        snackbar.view.setBackgroundColor(Color.TRANSPARENT)

        val snackbarView = snackbar.view as Snackbar.SnackbarLayout
        snackbarView.setPadding(0, 0, 0, 0)

        customView.findViewById<TextView>(R.id.message).text = message
        customView.findViewById<ImageView>(R.id.icon).setImageResource(R.drawable.ic_done)
        customView.findViewById<LinearLayout>(R.id.parent_view)
            .setBackgroundColor(
                ContextCompat.getColor(
                    mActivity.applicationContext,
                    R.color.green_500
                )
            )
        snackbarView.addView(customView, 0)
        return snackbar
    }

    private fun showSnackbarError(message: String, duration:Int): Snackbar {
        val view: View = mActivity.findViewById(android.R.id.content)
        val snackbar = Snackbar.make(view, "", duration)
        val customView = mActivity.layoutInflater.inflate(R.layout.snackbar_icon_text, null)
        snackbar.view.setBackgroundColor(Color.TRANSPARENT)

        val snackbarView = snackbar.view as Snackbar.SnackbarLayout
        snackbarView.setPadding(0, 0, 0, 0)

        customView.findViewById<TextView>(R.id.message).text = message
        customView.findViewById<ImageView>(R.id.icon).setImageResource(R.drawable.ic_close)
        customView.findViewById<LinearLayout>(R.id.parent_view)
            .setBackgroundColor(
                ContextCompat.getColor(
                    mActivity.applicationContext,
                    R.color.red_600
                )
            )
        snackbarView.addView(customView, 0)
        return snackbar
    }

    private fun showSnackbarInfo(message: String, duration:Int): Snackbar {
        val view: View = mActivity.findViewById(android.R.id.content)
        val snackbar = Snackbar.make(view, "", duration)
        val customView = mActivity.layoutInflater.inflate(R.layout.snackbar_icon_text, null)

        snackbar.view.setBackgroundColor(Color.TRANSPARENT)

        val snackbarView = snackbar.view as Snackbar.SnackbarLayout
        snackbarView.setPadding(0, 0, 0, 0)

        customView.findViewById<TextView>(R.id.message).text = message
        customView.findViewById<ImageView>(R.id.icon).setImageResource(R.drawable.ic_error_outline)
        customView.findViewById<LinearLayout>(R.id.parent_view)
            .setBackgroundColor(
                ContextCompat.getColor(
                    mActivity.applicationContext,
                    R.color.blue_500
                )
            )
        snackbarView.addView(customView, 0)
        return snackbar
    }

}