package com.galee.core.util.widget

import android.app.Activity
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.galee.core.R

class ToastCustom(activity: Activity) {
    private var mActivity: Activity = activity

    fun get(type: String,
            message: String,
            duration: Int = Toast.LENGTH_LONG): Toast {
        when (type) {
            "success" -> return showToastSuccess(message, duration)
            "danger" -> return showToastError(message, duration)
            "info" -> return showToastInfo(message, duration)
        }
        return Toast.makeText(mActivity.applicationContext, message, duration)
    }

    private fun showToastSuccess(message: String, duration: Int): Toast {
        val toast = Toast(mActivity.applicationContext)
        toast.duration = duration

        val customView = mActivity.layoutInflater.inflate(R.layout.toast_icon_text, null)

        customView.findViewById<TextView>(R.id.message).text = message
        customView.findViewById<ImageView>(R.id.icon).setImageResource(R.drawable.ic_done)
        customView.findViewById<CardView>(R.id.parent_view)
            .setCardBackgroundColor(ContextCompat.getColor(mActivity.applicationContext, R.color.green_500))
        toast.view = customView
        return toast
    }

    private fun showToastError(message: String, duration: Int): Toast {
        val toast = Toast(mActivity.applicationContext)
        toast.duration = duration

        val customView = mActivity.layoutInflater.inflate(R.layout.toast_icon_text, null)

        customView.findViewById<TextView>(R.id.message).text = message
        customView.findViewById<ImageView>(R.id.icon).setImageResource(R.drawable.ic_error_outline)
        customView.findViewById<CardView>(R.id.parent_view)
            .setCardBackgroundColor(ContextCompat.getColor(mActivity.applicationContext, R.color.red_600))
        toast.view = customView
        return toast
    }

    private fun showToastInfo(message: String, duration: Int): Toast {
        val toast = Toast(mActivity.applicationContext)
        toast.duration = duration

        val customView = mActivity.layoutInflater.inflate(R.layout.toast_icon_text, null)

        customView.findViewById<TextView>(R.id.message).text = message
        customView.findViewById<ImageView>(R.id.icon).setImageResource(R.drawable.ic_error_outline)
        customView.findViewById<CardView>(R.id.parent_view)
            .setCardBackgroundColor(ContextCompat.getColor(mActivity.applicationContext, R.color.blue_600))
        toast.view = customView
        return toast
    }
}