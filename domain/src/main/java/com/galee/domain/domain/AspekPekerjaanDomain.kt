package com.galee.domain.domain

data class AspekPekerjaanDomain(
    var id: Int = 0,
    var noPermit: String = "",
    var date: String = "",
    var time: String = "",
    var idArea: Int = 0,
    var idUnitKerja: Int = 0,
    var idSubUnitKerja: Int = 0,
    var nmUnitKerja: String = "",
    var pekerjaan: String = "",
    var peminta: String = "",
    var latitude: String = "",
    var longitude: String = "",
    var recommendGas: String = "",
    var ukurGas: String = "",
    var error: String = "",
    var status: Int = 0,
    var nmArea: String = "",
    var nmDepartemen: String = ""
)