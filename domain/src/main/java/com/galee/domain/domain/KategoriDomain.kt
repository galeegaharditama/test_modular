package com.galee.domain.domain

data class KategoriDomain(var id: Int, var title: String, var logo: String, var isSelected: Boolean)
