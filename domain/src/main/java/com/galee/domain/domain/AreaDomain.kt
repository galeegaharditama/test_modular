package com.galee.domain.domain

data class AreaDomain(var id: Int, var name: String){

    override fun toString(): String = name
}
