package com.galee.domain.domain

data class UserDomain(
    val idUser: Int,
    val idJabatan: String,
    val jabatanUser: String,
    val namaUser: String,
    val phone: String,
    val username: String,
    val tokenType: String,
    val expiresIn: Long,
    val accessToken: String,
    val refreshToken: String,
    val masaBerlaku: String
)