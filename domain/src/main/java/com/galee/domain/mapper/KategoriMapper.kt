package com.galee.domain.mapper

import com.galee.domain.BaseMapperResponseToDomain
import com.galee.domain.domain.KategoriDomain
import com.galee.domain.remote.response.KategoriResponse

open class KategoriMapper : BaseMapperResponseToDomain<KategoriResponse, KategoriDomain> {

    override fun mapResponseToDomain(response: KategoriResponse): KategoriDomain {
        if (response.logo.isEmpty()){
            response.logo = "R.drawable.ic_image_error"
        }
        return KategoriDomain(
            id = response.id, title = response.title, logo = response.logo, isSelected = response.isSelected
        )
    }
}