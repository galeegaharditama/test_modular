package com.galee.domain.mapper

import com.galee.domain.BaseMapperResponseToDomain
import com.galee.domain.domain.AspekPekerjaanDomain
import com.galee.domain.remote.response.AspekPekerjaanResponse
import java.util.*

open class AspekPekerjaanMapper :
    BaseMapperResponseToDomain<AspekPekerjaanResponse, AspekPekerjaanDomain> {

    override fun mapResponseToDomain(response: AspekPekerjaanResponse): AspekPekerjaanDomain {
        val currentCalendar = Calendar.getInstance()
        val date = response.date?.let {
            if (response.date.isNullOrBlank()) currentCalendar.timeInMillis.toString()
            else it
        } ?: currentCalendar.timeInMillis.toString()

        val time = response.time?.let {
            if (response.time.isNullOrBlank()) currentCalendar.timeInMillis.toString()
            else it
        } ?: currentCalendar.timeInMillis.toString()

        return AspekPekerjaanDomain(
            id = response.id ?: 0,
            noPermit = response.noPermit ?: "",
            date = date,
            time = time,
            idArea = response.idArea,
            idUnitKerja = response.idUnitKerja,
            idSubUnitKerja = response.idSubUnitKerja,
            nmUnitKerja = response.nmUnitKerja ?: "",
            pekerjaan = response.pekerjaan ?: "",
            peminta = response.peminta ?: "",
            latitude = response.latitude ?: "",
            longitude = response.longitude ?: "",
            recommendGas = response.recommendGas ?: "",
            ukurGas = response.ukurGas ?: "",
            error = response.error ?: "",
            status = response.status,
            nmArea = response.nmArea ?: "",
            nmDepartemen = response.nmDepartemen ?: ""
        )
    }
}