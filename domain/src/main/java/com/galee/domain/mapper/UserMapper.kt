package com.galee.domain.mapper

import com.galee.domain.BaseMapperResponseToDomain
import com.galee.domain.domain.UserDomain
import com.galee.domain.remote.response.UserResponse

open class UserMapper : BaseMapperResponseToDomain<UserResponse, UserDomain>{

    override fun mapResponseToDomain(response: UserResponse): UserDomain {
        return UserDomain(
            idUser = response.idUser.toInt(),
            idJabatan = response.idJabatan,
            jabatanUser = response.jabatanUser,
            namaUser = response.namaUser,
            phone = response.phone,
            username = response.username,
            tokenType = response.tokenType,
            expiresIn = response.expiresIn,
            accessToken = response.accessToken,
            refreshToken = response.refreshToken,
            masaBerlaku = response.masaBerlaku ?: ""
        )
    }
}