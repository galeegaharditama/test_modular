package com.galee.domain.mapper

import com.galee.domain.BaseMapperResponseToDomain
import com.galee.domain.domain.AreaDomain
import com.galee.domain.remote.response.AreaModel

open class AreaMapper : BaseMapperResponseToDomain<AreaModel, AreaDomain> {

    override fun mapResponseToDomain(response: AreaModel): AreaDomain {
        return AreaDomain(
            id = response.id ?: 0,
            name = response.name ?: ""
        )
    }
}