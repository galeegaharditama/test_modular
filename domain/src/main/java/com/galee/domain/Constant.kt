package com.galee.domain

import android.Manifest

object Constant {
    const val DATABASE_NAME = "eposh.db" // nama database
    const val APP_NAME = "Modular App" // nama aplikasi
    const val APP_ID = "com.galee.modular" // ambil dari manifest app
    var BASE_URL = if (BuildConfig.DEBUG) BuildConfig.URL_DEBUG else BuildConfig.URL_PROD
    //  TAMBAHKAN DISINI JIKA ADA VARIABEL CONST BARU
    val PERMISSIONS = mutableListOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
}