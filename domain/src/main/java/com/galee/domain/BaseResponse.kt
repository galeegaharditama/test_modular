package com.galee.domain

data class BaseResponse<out T>(val status: Status, var case: Int? = 0, val data: T?, val message: String) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING,
    }

    companion object {
        fun <T> loading(data: T? = null): BaseResponse<T> {
            return BaseResponse(Status.LOADING, 0, data, message = "")
        }

        fun <T> success(message: String, data: T): BaseResponse<T> {
            return BaseResponse(Status.SUCCESS, 0, data, message)
        }

        fun <T> error(message: String, case: Int = 0, data: T? = null): BaseResponse<T> {
            return BaseResponse(Status.ERROR, case, data, message)
        }
    }
}