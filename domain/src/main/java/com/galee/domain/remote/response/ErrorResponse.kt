package com.galee.domain.remote.response

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
	@SerializedName("name")
	val name: String? = null,
	@SerializedName("message")
	val message: Boolean? = null,
	@SerializedName("errors")
	val errors: Errors? = null,
	@SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Errors(
	@SerializedName("status_code")
	val statusCode: String? = null,

	@SerializedName("message")
	val message: String? = null
)