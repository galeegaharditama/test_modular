package com.galee.domain.remote.response

import com.google.gson.annotations.SerializedName

data class MasterKategoriResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("count_data")
    var count_data: Int,
    @SerializedName("error")
    var error: Boolean,
    @SerializedName("data")
    var data: List<KategoriResponse>? = null
)

data class KategoriResponse(
    @SerializedName("ID_FORM_KATEGORI")
    var id: Int,
    @SerializedName("KATEGORI_FORM")
    var title: String,
    @SerializedName("PATH_ICON")
    var logo: String,
    var isSelected: Boolean = false
)