package com.galee.domain.remote.response

import com.google.gson.annotations.SerializedName

data class AspekPekerjaanResponse(
    @SerializedName("ID_PERMIT")
    var id: Int? = 0,
    @SerializedName("NO_PERMIT")
    var noPermit: String? = "",
    @SerializedName("DATE")
    var date: String? = "",
    @SerializedName("TIME")
    var time: String? = "",
    @SerializedName("ID_AREA")
    var idArea: Int = 0,
    @SerializedName("ID_DEPARTEMEN")
    var idUnitKerja: Int = 0,
    @SerializedName("ID_BAGIAN")
    var idSubUnitKerja: Int = 0,
    @SerializedName("NM_BAGIAN")
    var nmUnitKerja: String? = "",
    @SerializedName("PEKERJAAN")
    var pekerjaan: String? = "",
    @SerializedName("PEMINTA")
    var peminta: String? = "",
    @SerializedName("LATITUDE")
    var latitude: String? = "",
    @SerializedName("LONGITUDE")
    var longitude: String? = "",
    @SerializedName("RECOMMEND_GAS")
    var recommendGas: String? = "",
    @SerializedName("UKUR_GAS")
    var ukurGas: String? = "",
    @SerializedName("ERROR")
    var error: String? = null,
    @SerializedName("STATUS")
    var status: Int = 0,
    @SerializedName("NM_AREA")
    var nmArea: String? = "",
    @SerializedName("NM_DEPARTEMEN")
    var nmDepartemen: String? = ""
)