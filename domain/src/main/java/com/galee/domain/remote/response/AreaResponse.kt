package com.galee.domain.remote.response

import com.google.gson.annotations.SerializedName

data class AreaResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("count_data")
    var count_data: Int,
    @SerializedName("error")
    var error: Boolean,
    @SerializedName("data")
    var data: List<AreaModel>? = null
)

data class AreaModel(
    @SerializedName("ID_AREA")
    var id: Int?,
    @SerializedName("NM_AREA")
    var name: String?
)