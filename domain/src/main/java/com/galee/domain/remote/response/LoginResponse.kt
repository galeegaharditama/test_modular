package com.galee.domain.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("message")
    @Expose
    var message: String,
    @SerializedName("error")
    @Expose
    var error: Boolean,
    @SerializedName("data")
    @Expose
    var data: UserResponse? = null
)

data class UserResponse(
	@SerializedName("id_user")
	val idUser: Long,
	@SerializedName("id_jabatan")
	val idJabatan: String,
	@SerializedName("jabatan_user")
	val jabatanUser: String,
	@SerializedName("nama_user")
	val namaUser: String,
	@SerializedName("phone")
	val phone: String,
	@SerializedName("username")
	val username: String,
	@SerializedName("token_type")
	val tokenType: String,
	@SerializedName("expires_in")
	val expiresIn: Long,
	@SerializedName("access_token")
	val accessToken: String,
	@SerializedName("refresh_token")
	val refreshToken: String,
	@SerializedName("masa_berlaku")
	val masaBerlaku: String?
)