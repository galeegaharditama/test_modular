package com.galee.domain

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.galee.domain.db.DatabaseHelper
import com.galee.domain.mapper.AreaMapper
import com.galee.domain.mapper.AspekPekerjaanMapper
import com.galee.domain.mapper.KategoriMapper
import com.galee.domain.mapper.UserMapper
import com.galee.domain.remote.internal.ConnectivityInterceptorImpl
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val REQUEST_TIMEOUT = 60

val domainModule = module {
    single { UserMapper() }
    single { KategoriMapper() }
    single { AreaMapper() }
    single { AspekPekerjaanMapper() }
}

val databaseModule = module {
    single {
        DatabaseHelper.getInstance(get())
    }
}

val networkModule = module {
    single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }
    single<OkHttpClient> {
        OkHttpClient.Builder()
            .apply {
                val profil =
                    androidContext().getSharedPreferences("profil.conf", Context.MODE_PRIVATE)
                val token = profil.getString("access_token", null)
                addInterceptor {
                    val original = it.request()
                    val requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                    if (token != null) requestBuilder.addHeader("Authorization", "Bearer $token")
                    val request = requestBuilder.build()
                    it.proceed(request)
                }
                if (BuildConfig.DEBUG) {
                    addInterceptor(get<HttpLoggingInterceptor>())
                }
            }
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(ConnectivityInterceptorImpl(get()))
            .addNetworkInterceptor(StethoInterceptor())
            .build()
    }

    single<Retrofit> {
        val sharedPreferences =
            androidContext().getSharedPreferences("setting_api.conf", Context.MODE_PRIVATE)
        Retrofit.Builder()
            .client(get())
            .baseUrl(
                if (sharedPreferences.getString("api", "").isNullOrBlank()) sharedPreferences.getString("api", "")
                else Constant.BASE_URL
            )
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
val networkModuleFAN = module {
    single {
        val okHttpClient = OkHttpClient().newBuilder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()
        AndroidNetworking.initialize(get(), okHttpClient)
    }
}