package com.galee.domain.db.entity

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import androidx.room.ForeignKey.NO_ACTION

@Entity(tableName = "image_vt")
data class ImageVerifyTruckEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0,
    @ColumnInfo(name = "image_id")
    var image_id: String = "",
    @ColumnInfo(name = "image_name")
    var imageName: String,
    @ColumnInfo(name = "dailycode")
    var dailycode: String,
    @ColumnInfo(name = "picture")
    var picture: String,
    @ColumnInfo(name = "upload_date")
    var uploadDate: String,
    @ColumnInfo(name = "capture_date")
    var captureDate: String,
    @ColumnInfo(name = "user")
    var userName: String,
    @ColumnInfo(name = "user_id")
    var userId: Int,
    @ColumnInfo(name = "status")
    var status: Int,
    /*1 = SUDAH TERUPLOAD; 0 = BELUM TERUPLOAD*/
    @ColumnInfo(name = "sync")
    var sync: Int
)

@Dao
interface ImageVerifyTruckEntityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(image: ImageVerifyTruckEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(images: List<ImageVerifyTruckEntity>)

    @Update
    suspend fun update(image: ImageVerifyTruckEntity)

    @Query("SELECT * FROM image_vt WHERE dailycode = :dailycode AND status = 1 ORDER BY capture_date DESC")
    suspend fun all(dailycode: String): List<ImageVerifyTruckEntity>

    @Query("SELECT * FROM image_vt WHERE sync = 0 AND dailycode = :dailycode ORDER BY capture_date DESC")
    suspend fun allUnsync(dailycode: String = ""): List<ImageVerifyTruckEntity>

    @Query("SELECT * FROM image_vt WHERE dailycode = :dailycode AND sync = :sync ORDER BY capture_date DESC")
    suspend fun allSyncOrUnSync(dailycode: String, sync: Int): List<ImageVerifyTruckEntity>

    @Query("SELECT * FROM image_vt WHERE id = :id ORDER BY id DESC")
    suspend fun one(id: Int): ImageVerifyTruckEntity

    @Transaction
    @Query("DELETE FROM image_vt WHERE image_id = :id")
    suspend fun delete(id:Int)
}