package com.galee.domain.db

import android.content.Context
import android.os.Environment
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.galee.domain.Constant
import com.galee.domain.db.entity.*
import java.io.File

@Database(entities = [UserEntity::class, VerifyTruckEntity::class, ImageVerifyTruckEntity::class], version = 2, exportSchema = true)
abstract class DatabaseHelper : RoomDatabase() {

    abstract fun userDao(): UserEntityDao
    abstract fun verifyTruckDao(): VerifyTruckEntityDao
    abstract fun imageVerifyTruckDao(): ImageVerifyTruckEntityDao

    companion object {
        @Volatile
        private var INSTANCE: DatabaseHelper? = null

        fun getInstance(context: Context): DatabaseHelper? {
            if (INSTANCE == null) {
                synchronized(DatabaseHelper::class) {
                    if (INSTANCE == null) {
                        INSTANCE = buildDatabase(context)
                    }
                }
            }
            return INSTANCE
        }

        private fun buildDatabase(applicationContext: Context): DatabaseHelper {
            val path =
                Environment.getExternalStorageDirectory().toString() + "/${Constant.APP_NAME}/database/"
            println(path)

            setFolderDatabase(path)

            return Room.databaseBuilder(
                applicationContext.applicationContext, DatabaseHelper::class.java,
                path + Constant.DATABASE_NAME)
                .addMigrations(MIGRATION_1_2
//                    MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6,
//                        MIGRATION_6_7, MIGRATION_8, MIGRATION_9, MIGRATION_10, MIGRATION_11, MIGRATION_12, MIGRATION_13,
//                        MIGRATION_14, MIGRATION_15, MIGRATION_16, MIGRATION_17, MIGRATION_18, MIGRATION_19, MIGRATION_20,
//                        MIGRATION_21, MIGRATION_22
                        )
                .build()

        }

        private fun setFolderDatabase(path: String): Boolean {
            val folder = File(path)
            var success = true
            if (!folder.exists()) {
                success = folder.mkdirs()
            }
            if (success) {
                println("Sukses buat folder database!")
            } else {
                println("Tidak bisa membuat folder !")
            }
            return success
        }

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS user (`user_id` INTEGER, `username` TEXT, `password` TEXT, `name` TEXT, `time` TEXT, `token` TEXT, PRIMARY KEY(`user_id`))")
                database.execSQL("CREATE TABLE IF NOT EXISTS verifytruck (`code` TEXT NOT NULL, `nopol` TEXT, `type_truck` TEXT, `ekspedisi_real` TEXT, `truck_masuk` TEXT, `truck_keluar` TEXT, `remark` TEXT, `remark_by_device` TEXT, `status` TEXT, `distributor_code` TEXT, `distributor_name` TEXT, `created_at` TEXT, `updated_at` TEXT, PRIMARY KEY(`code`))")
                database.execSQL("CREATE TABLE IF NOT EXISTS image_vt (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `image_id` TEXT NOT NULL, `image_name` TEXT NOT NULL, `dailycode` TEXT NOT NULL, `picture` TEXT NOT NULL, `upload_date` TEXT NOT NULL, `capture_date` TEXT NOT NULL, `user` TEXT NOT NULL, `user_id` INTEGER NOT NULL, `status` INTEGER NOT NULL, `sync` INTEGER NOT NULL)")

                database.execSQL("ALTER TABLE image_vt RENAME TO image_vt_old")
                database.execSQL("CREATE TABLE IF NOT EXISTS image_vt (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `image_id` TEXT NOT NULL, `image_name` TEXT NOT NULL, `dailycode` TEXT NOT NULL, `picture` TEXT NOT NULL, `upload_date` TEXT NOT NULL, `capture_date` TEXT NOT NULL, `user` TEXT NOT NULL, `user_id` INTEGER NOT NULL, `status` INTEGER NOT NULL, `sync` INTEGER NOT NULL)")
                database.execSQL("INSERT INTO image_vt (ID, IMAGE_ID, IMAGE_NAME, DAILYCODE, PICTURE, UPLOAD_DATE, CAPTURE_DATE, USER, USER_ID, STATUS, SYNC) SELECT ID, IMAGE_ID, IMAGE_NAME, DAILYCODE, PICTURE, UPLOAD_DATE, CAPTURE_DATE, USER, USER_ID, STATUS, SYNC FROM image_vt_old")
                database.execSQL("DROP TABLE image_vt_old")
            }
        }
    }
}