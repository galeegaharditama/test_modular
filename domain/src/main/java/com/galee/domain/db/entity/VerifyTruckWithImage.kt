package com.galee.domain.db.entity

import androidx.room.Embedded
import androidx.room.Relation
import com.galee.domain.db.entity.ImageVerifyTruckEntity
import com.galee.domain.db.entity.VerifyTruckEntity


data class VerifyTruckWithImage(
    @Embedded
    val verifyTruck: VerifyTruckEntity,
    @Relation(
        parentColumn = "code",
        entityColumn = "dailycode",
        entity = ImageVerifyTruckEntity::class
    )
    val images: MutableList<ImageVerifyTruckEntity> = mutableListOf()
)