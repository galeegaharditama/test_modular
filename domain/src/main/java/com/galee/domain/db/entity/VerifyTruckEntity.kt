package com.galee.domain.db.entity

import androidx.room.*

@Entity(tableName = "verifytruck")
data class VerifyTruckEntity(
    @PrimaryKey
    @ColumnInfo(name = "code")
    var code: String = "",
    @ColumnInfo(name = "nopol")
    var nopol: String? = null,
    @ColumnInfo(name = "type_truck")
    var typeTruck: String? = null,
    @ColumnInfo(name = "ekspedisi_real")
    var ekspedisi_real: String? = null,
    @ColumnInfo(name = "truck_masuk")
    var truckMasuk: String? = null,
    @ColumnInfo(name = "truck_keluar")
    var truckKeluar: String? = null,
    @ColumnInfo(name = "remark")
    var remark: String? = null,
    @ColumnInfo(name = "remark_by_device")
    var remarkByDevice: String? = null,
    @ColumnInfo(name = "status")
    var status: String? = null,
    @ColumnInfo(name = "distributor_code")
    var distributorCode: String? = null,
    @ColumnInfo(name = "distributor_name")
    var distributorName: String? = null,
    @ColumnInfo(name = "created_at")
    var createdAt: String? = null,
    @ColumnInfo(name = "updated_at")
    var updatedAt: String? = null
)

@Dao
interface VerifyTruckEntityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(verifytruck: VerifyTruckEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(verifytrucks: List<VerifyTruckEntity>)

    @Update
    suspend fun update(verifytruck: VerifyTruckEntity)

    @Query("SELECT * FROM verifytruck WHERE code = :code ORDER BY created_at ASC")
    suspend fun one(code: String): VerifyTruckWithImage

    @Query("SELECT * FROM verifytruck ORDER BY created_at DESC")
    suspend fun all(): List<VerifyTruckWithImage>

    @Query("SELECT * FROM verifytruck WHERE code LIKE :query OR nopol LIKE :query ORDER BY created_at DESC")
    suspend fun all(query: String): List<VerifyTruckWithImage>
}