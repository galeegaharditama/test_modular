package com.galee.domain.db.entity

import androidx.room.*

@Entity(tableName = "user")
data class UserEntity(
    @PrimaryKey
    @ColumnInfo(name = "user_id")
    var id: Int? = null,
    @ColumnInfo(name = "username")
    var username: String? = null,
    @ColumnInfo(name = "password")
    var password: String? = null,
    @ColumnInfo(name = "name")
    var name: String? = null,
    @ColumnInfo(name = "time")
    var time: String? = null,
    @ColumnInfo(name = "token")
    var token: String? = null
) {
    override fun toString(): String = "$name"
}

@Dao
interface UserEntityDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: UserEntity)

    @Update
    suspend fun update(user: UserEntity)

    @Query("SELECT * FROM user WHERE username = :username AND password = :password ORDER BY user_id ASC")
    suspend fun one(username: String, password: String): MutableList<UserEntity>

    @Query("DELETE FROM user")
    suspend fun deleteAll()
}