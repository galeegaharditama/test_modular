package com.galee.domain

interface BaseMapperResponseToEntity<R, E> {

    fun mapResponseToEntity(response: R): E

    fun mapResponsesToListEntity(responses: List<R>): List<E> {
        val listEntity = mutableListOf<E>()
        responses.map { listEntity.add(mapResponseToEntity(it)) }
        return listEntity
    }
}

interface BaseMapperResponseToDomain<R, D> {
    fun mapResponseToDomain(response: R): D

    fun mapResponsesToListDomain(responses: List<R>): List<D> {
        val listDomain = mutableListOf<D>()
        responses.map { listDomain.add(mapResponseToDomain(it)) }
        return listDomain
    }
}

interface BaseMapperEntityToDomain<E, D> {
    fun mapDomainToEntity(domain: D): E
    fun mapEntityToDomain(entity: E): D

    fun mapEntitysToListDomain(entitys: List<E>): List<D> {
        val listDomain = mutableListOf<D>()
        entitys.map { listDomain.add(mapEntityToDomain(it)) }
        return listDomain
    }

    fun mapDomainsToListEntity(domains: List<D>): List<E> {
        val listEntity = mutableListOf<E>()
        domains.map { listEntity.add(mapDomainToEntity(it)) }
        return listEntity
    }
}

interface BaseMapperModelToDomain<M, D> {
    fun mapModelToDomain(model: M): D
    fun mapDomainToModel(domain: D): M

    fun mapModelsToListDomain(models: List<M>): List<D> {
        val listDomain = mutableListOf<D>()
        models.map { listDomain.add(mapModelToDomain(it)) }
        return listDomain
    }

    fun mapDomainsToListModel(domains: List<D>): List<M> {
        val listModels = mutableListOf<M>()
        domains.map { listModels.add(mapDomainToModel(it)) }
        return listModels
    }
}