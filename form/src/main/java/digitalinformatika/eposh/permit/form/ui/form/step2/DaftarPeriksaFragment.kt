package digitalinformatika.eposh.permit.form.ui.form.step2

import android.os.Bundle
import android.view.View
import com.galee.core.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel
import digitalinformatika.eposh.permit.form.R

class DaftarPeriksaFragment : BaseFragment() {

    companion object {
        const val TAG = "DaftarPeriksaFragment"
        fun newInstance(bundle: Bundle): DaftarPeriksaFragment {
            val fragment = DaftarPeriksaFragment()
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance() = DaftarPeriksaFragment()
    }

    private val vmDaftarPeriksa: DaftarPeriksaVM by viewModel()

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.fragment_daftar_periksa

    override fun onCreateUI(view: View, savedInstanceState: Bundle?) {

    }
}