package digitalinformatika.eposh.permit.form.data.remote

import android.content.Context
import com.galee.core.R as coreR
import com.galee.domain.BaseResponse
import com.galee.domain.domain.AreaDomain
import com.galee.domain.domain.AspekPekerjaanDomain
import com.galee.domain.mapper.AreaMapper
import com.galee.domain.mapper.AspekPekerjaanMapper
import com.galee.domain.remote.internal.util.NetworkUtil
import com.galee.domain.remote.response.AspekPekerjaanResponse
import com.galee.modular.data.remote.AppServices
import com.google.gson.Gson

class FormRepositoryImpl(
    private val context: Context,
    private val areaMapper: AreaMapper,
    private val aspekPekerjaanMapper: AspekPekerjaanMapper,
    private val service: AppServices
) : FormRepository {

    override suspend fun getArea(): MutableList<AreaDomain> {

        val response = service.masterAreaAsync().await()

        response.data?.let {
            return areaMapper.mapResponsesToListDomain(it).toMutableList()
//            return BaseResponse.success(response.message, areaMapper.mapResponsesToListDomain(it).toMutableList())
        }
        return mutableListOf()
//        return BaseResponse.error(context.resources.getString(coreR.string.base_error_no_data), case = 4)

    }

    override suspend fun setAspekPekerjaan(jsonAspek: String): AspekPekerjaanDomain {
        val response = Gson().fromJson<AspekPekerjaanResponse>(jsonAspek, AspekPekerjaanResponse::class.java)
        return aspekPekerjaanMapper.mapResponseToDomain(response)
    }
}