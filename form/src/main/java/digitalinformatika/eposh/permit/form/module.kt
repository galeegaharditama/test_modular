package digitalinformatika.eposh.permit.form

import com.galee.modular.data.remote.AppServices
import digitalinformatika.eposh.permit.form.data.remote.FormRepository
import digitalinformatika.eposh.permit.form.data.remote.FormRepositoryImpl
import digitalinformatika.eposh.permit.form.ui.form.FormVM
import digitalinformatika.eposh.permit.form.ui.form.step1.AspekPekerjaanVM
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

fun injectFeature() = loadFeature

private val loadFeature by lazy {
    loadKoinModules(
        arrayListOf(formModule, vmModule)
    )
}

val formModule = module {
//    single<AppServices> { get<Retrofit>().create(AppServices::class.java) }
    single { FormRepositoryImpl(get(), get(), get(), get()) as FormRepository }
}

val vmModule = module {
    viewModel { FormVM(get(), get()) }
    viewModel { AspekPekerjaanVM(get(), get()) }
}