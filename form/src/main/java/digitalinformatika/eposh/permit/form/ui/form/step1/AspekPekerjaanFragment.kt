package digitalinformatika.eposh.permit.form.ui.form.step1

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import com.galee.core.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel
import digitalinformatika.eposh.permit.form.R
import digitalinformatika.eposh.permit.form.ui.form.FormVM
import kotlinx.android.synthetic.main.fragment_aspek_pekerjaan.view.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import java.util.*
import androidx.lifecycle.Observer
import com.galee.core.util.*
import com.galee.core.util.widget.ErrorView
import com.galee.domain.BaseResponse
import com.galee.domain.Constant
import com.galee.domain.domain.AreaDomain
import com.galee.domain.domain.AspekPekerjaanDomain
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import com.galee.core.R as coreR

class AspekPekerjaanFragment : BaseFragment() {

    companion object {
        const val TAG = "AspekPekerjaanFragment"
        fun newInstance(bundle: Bundle): AspekPekerjaanFragment {
            val fragment = AspekPekerjaanFragment()
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance() = AspekPekerjaanFragment()
    }

    private val vmAspekPekerjaan: AspekPekerjaanVM by viewModel()
    private val vmForm: FormVM by sharedViewModel()
    private lateinit var adapterArea: ArrayAdapter<AreaDomain>
    private val mListArea = mutableListOf<AreaDomain>()
    private val currentCalendar = Calendar.getInstance()
    private var mAspekPekerjaan: AspekPekerjaanDomain = AspekPekerjaanDomain()

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.fragment_aspek_pekerjaan

    override fun onCreateUI(view: View, savedInstanceState: Bundle?) {
        view.aspek_lyt_swipe.setOnRefreshListener {
        }

        view.aspek_form_edt_date.setOnClickListener {
            val datePicker = DatePickerDialog.newInstance(
                { _: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                    currentCalendar.set(Calendar.YEAR, year)
                    currentCalendar.set(Calendar.MONTH, monthOfYear)
                    currentCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    mAspekPekerjaan.date = currentCalendar.timeInMillis.toString()
                    vmAspekPekerjaan.aspekDomain.value = mAspekPekerjaan
                    mView.aspek_form_edt_date.setText(currentCalendar.timeInMillis.getFormattedDateEvent())
                },
                currentCalendar.get(Calendar.YEAR),
                currentCalendar.get(Calendar.MONTH),
                currentCalendar.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.isThemeDark = false
            datePicker.accentColor = view.context.getColorCompat(coreR.color.colorPrimary)
            datePicker.setOkColor(view.context.getColorCompat(coreR.color.colorOnBackground))
            datePicker.setCancelColor(view.context.getColorCompat(coreR.color.colorOnBackground))
            activity?.run {
                datePicker.show(supportFragmentManager, "Datepickerdialog")
            }
        }

        view.aspek_form_edt_time.setOnClickListener {
            val timePicker = TimePickerDialog.newInstance(
                { _: TimePickerDialog?, hourOfDay: Int, minute: Int, _: Int ->
                    currentCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    currentCalendar.set(Calendar.MINUTE, minute)
                    currentCalendar.set(Calendar.AM_PM, currentCalendar.get(Calendar.AM_PM))
                    mAspekPekerjaan.time = currentCalendar.timeInMillis.toString()
                    vmAspekPekerjaan.aspekDomain.value = mAspekPekerjaan
                    mView.aspek_form_edt_time.setText(currentCalendar.timeInMillis.getFormattedTimeEvent())
                },
                currentCalendar.get(Calendar.HOUR_OF_DAY),
                currentCalendar.get(Calendar.MINUTE), true
            )
            timePicker.isThemeDark = false
            timePicker.accentColor = view.context.getColorCompat(coreR.color.colorPrimary)
            timePicker.setOkColor(view.context.getColorCompat(coreR.color.colorOnBackground))
            timePicker.setCancelColor(view.context.getColorCompat(coreR.color.colorOnBackground))
            activity?.run {
                timePicker.show(supportFragmentManager, "Timepickerdialog")
            }
        }

        view.aspek_form_edt_area.setOnItemClickListener { _, _, i, _ ->
            mAspekPekerjaan.idArea = mListArea[i].id
            mAspekPekerjaan.nmArea = mListArea[i].name
            vmAspekPekerjaan.aspekDomain.value = mAspekPekerjaan
        }

        view.btn_location.setOnClickListener {

        }

        adapterArea = ArrayAdapter(view.context, R.layout.item_simple_spinner, mListArea)
        adapterArea.setDropDownViewResource(R.layout.item_simple_spinner)
        view.aspek_form_edt_area.setAdapter(adapterArea)

        observeVM()
        onRunPermission(Constant.PERMISSIONS, 0)
    }

    override fun onAllPermissionGranted(level: Int) {
        super.onAllPermissionGranted(level)
        when(level){
            0 -> {
                vmAspekPekerjaan.getArea()
            }
        }
    }

    fun isValid(): Boolean {
        var isValid = true
        vmAspekPekerjaan.aspekDomain.value?.let {
            vmAspekPekerjaan.isValidate(it).forEach {
                if (isValid && !it.status) isValid = it.status
                val layout = activity?.findViewById<View>(it.layout)
                when (layout) {
                    is TextInputLayout -> {
                        layout.isErrorEnabled = when (it.status) {
                            false -> true
                            true -> false
                        }
                        layout.error = it.message
                    }
                    /*is TextView -> {
                        when (it.status) {
                            false -> layout.visible()
                            true -> layout.gone()
                        }
                        layout.text = it.message
                    }*/
                    else -> {
                        println("else $layout")
                    }
                }
            }
        }
        return isValid
    }

    fun getValue(): String {
        mAspekPekerjaan.pekerjaan = mView.aspek_form_edt_pekerjaan?.text.toString()
        mAspekPekerjaan.peminta = mView.aspek_form_edt_peminta?.text.toString()
        mAspekPekerjaan.latitude = mView.aspek_form_edt_LA?.text.toString()
        mAspekPekerjaan.longitude = mView.aspek_form_edt_LG?.text.toString()
        return Gson().toJson(vmAspekPekerjaan.aspekDomain.value)
    }

    private fun observeVM(){
        vmForm.jsonAspek.observe(this, Observer {
            println("json Aspek $it")
            if (!it.isNullOrBlank()) vmAspekPekerjaan.setAspekPekerjaan(it)
        })

        vmAspekPekerjaan.uiState.observe(this, Observer { state ->
            when(state.status){
                BaseResponse.Status.LOADING -> {
                    mView.aspek_progress.visible()
                    mView.aspek_lyt_error.gone()
                    mView.aspek_lyt_swipe.gone()
                }
                BaseResponse.Status.SUCCESS -> {
                    mView.aspek_progress.gone()
                    mView.aspek_lyt_swipe.visible()
                }
                BaseResponse.Status.ERROR -> {
                    mView.aspek_progress.gone()

                }
            }
        })

        vmAspekPekerjaan.aspekDomain.observe(this, Observer {domain ->
            mAspekPekerjaan = domain
            if (domain.date.isNotBlank())
                mView.aspek_form_edt_date.setText(domain.date.toLong().getFormattedDateEvent())
            if (domain.time.isNotBlank())
                mView.aspek_form_edt_time.setText(domain.time.toLong().getFormattedTimeEvent())

            mView.aspek_form_edt_bagian.setText(domain.nmUnitKerja)
            mView.aspek_form_edt_pekerjaan.setText(domain.pekerjaan)
            mView.aspek_form_edt_peminta.setText(domain.peminta)
            mView.aspek_form_edt_LA.setText(domain.latitude)
            mView.aspek_form_edt_LG.setText(domain.longitude)
        })

        vmAspekPekerjaan.areaState.observe(this, Observer {
            it?.let {
                mListArea.clear()
                mListArea.add(AreaDomain(0, "Silahkan Pilih Area"))
                mListArea.addAll(it)
                adapterArea.notifyDataSetChanged()
                mListArea.forEachIndexed { index, areaDomain ->
                    if (vmAspekPekerjaan.aspekDomain.value?.idArea == areaDomain.id)
                        mView.aspek_form_edt_area.setSelection(index)
                }
            } ?: run {
                isErrorResponse(4, "Terjadi Kesalahan", 1)
            }
        })
    }

    private fun isErrorResponse(case: Int, msg: String?, type: Int) {
        when (type) {
            1 -> { // ERROR LOAD AREA
                mView.aspek_form_lyt_area.error = msg
            }
            2 -> {
                mView.aspek_lyt_error.setView(true, msg, case, object : ErrorView.ErrorListener{
                    override fun onReloadData() {

                    }
                })
                mView.aspek_lyt_error.visible()
                mView.aspek_lyt_swipe.gone()
            }
        }
    }
}