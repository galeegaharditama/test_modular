package digitalinformatika.eposh.permit.form.data.remote

import com.galee.domain.domain.AreaDomain
import com.galee.domain.domain.AspekPekerjaanDomain

interface FormRepository {
    suspend fun getArea(): MutableList<AreaDomain>
    suspend fun setAspekPekerjaan(jsonAspek: String): AspekPekerjaanDomain
}