package digitalinformatika.eposh.permit.form.ui.form

import android.graphics.PorterDuff
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.galee.core.BaseActivity
import com.galee.core.util.invisible
import com.galee.core.util.visible
import org.koin.android.viewmodel.ext.android.viewModel
import digitalinformatika.eposh.permit.form.R
import com.galee.core.R as coreR
import digitalinformatika.eposh.permit.form.injectFeature
import digitalinformatika.eposh.permit.form.ui.form.step1.AspekPekerjaanFragment
import kotlinx.android.synthetic.main.activity_form.*
import java.util.*

class FormActivity : BaseActivity() {

    companion object {
        const val TAG = "FormActivity"
        const val TAG_SCENARIO = "SCENARIO"
        const val CATEGORY = "CATEGORY"
    }

    private val vmForm: FormVM by viewModel()
    private var currentTAB = 0
    private var scenario = 0 // 0: REVIEW; 1: CREATE; 2: UPDATE

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.activity_form
    override fun onCreateUI(savedInstanceState: Bundle?) {
        injectFeature()

        toolbar.inflateMenu(R.menu.form_menu)
        toolbar.setOnMenuItemClickListener { menu ->
            when(menu.itemId){
                R.id.btn_draft -> {
                    showToastMessage("draft")
                    return@setOnMenuItemClickListener true
                }
                else -> false
            }
        }
        toolbar.setNavigationIcon(coreR.drawable.ic_close)
        toolbar.navigationIcon?.setColorFilter(ContextCompat.getColor(this, coreR.color.white), PorterDuff.Mode.SRC_ATOP)
        toolbar.setNavigationOnClickListener { askExit() }
        toolbar.title = "Form Permit"

        vmForm.jsonKategori.value = intent.getStringExtra(CATEGORY)
        scenario = intent.getIntExtra(TAG_SCENARIO, 0)
        if (scenario == 0){
            vmForm.isReview.value = true
        }

        btn_next.setOnClickListener {
            if (currentTAB == 4) {
                askSave()
                return@setOnClickListener
            }
            if (isCanNextStep()) {
                setJSONForm()
                setSubToolbar("+")
                setStateVM()
            }
        }

        btn_prev.setOnClickListener {
            onBackPressed()
        }

        observeVM()
    }

    override fun onBackPressed() {
        val fragmentList = supportFragmentManager.fragments
        if (supportFragmentManager.backStackEntryCount > 0){
            val contains =
                fragmentList[fragmentList.size - 1].javaClass.simpleName.toLowerCase(Locale.getDefault()) in arrayOf(
                    "bagianfragment", "slideimagefragment"
                )
            if (!contains){
                setJSONForm()
                setSubToolbar("-")
            }
            supportFragmentManager.popBackStack()
            return
        }
        askExit()
        return
    }

    private fun observeVM(){
        vmForm.formState.observe(this, Observer { state ->
            when (state) {
                is DefaultState -> {
                    vmForm.formState.value = Step1(vmForm.jsonKategori.value ?: "")
                }
                is Step1 -> {
                    currentTAB = 0
                    setSubToolbar(null)
                    val frag = supportFragmentManager.findFragmentByTag(AspekPekerjaanFragment.TAG)
                    if (frag == null) setContent()
                }

                /*is Step2 -> {
                    currentTAB = 1
                    setSubToolbar(null)
                    val frag = supportFragmentManager.findFragmentByTag(DaftarPeriksaFragment.TAG)
                    if (frag == null) setContent()
                }

                is Step3 -> {
                    currentTAB = 2
                    setSubToolbar(null)
                    val frag = supportFragmentManager.findFragmentByTag(PemeriksaanGasFragment.TAG)
                    if (frag == null) setContent()
                }

                is Step4 -> {
                    currentTAB = 3
                    setSubToolbar(null)
                    val frag = supportFragmentManager.findFragmentByTag(AlatPelindungFragment.TAG)
                    if (frag == null) setContent()
                }

                is Step5 -> {
                    currentTAB = 4
                    setSubToolbar(null)
                    val frag = supportFragmentManager.findFragmentByTag(ReviewFormFragment.TAG)
                    if (frag == null) setContent()
                }*/
            }
        })
    }

    private fun setStateVM(){
        when (currentTAB) {
            0 -> {
                vmForm.formState.value = Step1(
                    vmForm.jsonKategori.value ?: ""
                )
            }
            1 -> {
                vmForm.formState.value = Step2(
                    vmForm.jsonKategori.value ?: "",
                    vmForm.jsonAspek.value ?: ""
                )
            }
            2 -> {
                vmForm.formState.value = Step3(
                    vmForm.jsonKategori.value ?: "",
                    vmForm.jsonAspek.value ?: "",
                    vmForm.jsonPeriksa.value ?: ""
                )
            }
            3 -> {
                vmForm.formState.value = Step4(
                    vmForm.jsonKategori.value ?: "",
                    vmForm.jsonAspek.value ?: "",
                    vmForm.jsonPeriksa.value ?: "",
                    vmForm.jsonGas.value ?: ""
                )
            }
            4 -> {
                vmForm.formState.value = Step5(
                    vmForm.jsonKategori.value ?: "",
                    vmForm.jsonAspek.value ?: "",
                    vmForm.jsonPeriksa.value ?: "",
                    vmForm.jsonGas.value ?: "",
                    vmForm.jsonPelindung.value ?: ""
                )
            }
        }
    }

    private fun setContent() {
        when (currentTAB) {
            0 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.form_content, AspekPekerjaanFragment.newInstance(), AspekPekerjaanFragment.TAG)
//                    .addToBackStack(null)
                    .commit()
            }
            /*1 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_form, DaftarPeriksaFragment.newInstance(), DaftarPeriksaFragment.TAG)
                    .addToBackStack(FormActivity.TAG)
                    .commit()
            }
            2 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_form, PemeriksaanGasFragment.newInstance(), PemeriksaanGasFragment.TAG)
                    .addToBackStack(FormActivity.TAG)
                    .commit()
            }
            3 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_form, AlatPelindungFragment.newInstance(), AlatPelindungFragment.TAG)
                    .addToBackStack(FormActivity.TAG)
                    .commit()
            }
            4 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_form, ReviewFormFragment.newInstance(), ReviewFormFragment.TAG)
                    .addToBackStack(FormActivity.TAG)
                    .commit()
            }*/
        }
    }

    private fun setJSONForm() {
        val fragmentList = supportFragmentManager.fragments
        when (currentTAB) {
            0 -> {
                vmForm.jsonAspek.value = (supportFragmentManager.fragments[fragmentList.size - 1] as AspekPekerjaanFragment).getValue()
                println("${vmForm.jsonAspek.value}")
            }
            /*1 -> {
                vmForm.jsonPeriksa.value = (supportFragmentManager.fragments[fragmentList.size - 1] as DaftarPeriksaFragment).getValue()
                println("${vmForm.jsonPeriksa.value}")
            }
            2 -> {
                vmForm.jsonGas.value = (supportFragmentManager.fragments[fragmentList.size - 1] as PemeriksaanGasFragment).getValue()
                println("${vmForm.jsonGas.value}")
            }
            3 -> {
                vmForm.jsonPelindung.value = (supportFragmentManager.fragments[fragmentList.size - 1] as AlatPelindungFragment).getValue()
                println("${vmForm.jsonPelindung.value}")
            }*/
        }
    }

    private fun isCanNextStep(): Boolean =
        when (currentTAB) {
            0 -> {
                val fragmentList = supportFragmentManager.fragments
                (supportFragmentManager.fragments[fragmentList.size - 1] as AspekPekerjaanFragment).isValid()
            }
            /*1 -> {
                val fragmentList = supportFragmentManager.fragments
                (supportFragmentManager.fragments[fragmentList.size - 1] as DaftarPeriksaFragment).isValid()
            }
            2 -> {
                val fragmentList = supportFragmentManager.fragments
                (supportFragmentManager.fragments[fragmentList.size - 1] as PemeriksaanGasFragment).isValid()
            }
            3 -> {
                val fragmentList = supportFragmentManager.fragments
                (supportFragmentManager.fragments[fragmentList.size - 1] as AlatPelindungFragment).isValid()
            }*/
            else -> false
        }

    private fun setSubToolbar(operation: String?) {
        operation?.let { currentTAB = if (it == "+") currentTAB + 1 else currentTAB - 1 }
        btn_next.text = getString(R.string.form_btn_next)
        when (currentTAB) {
            0 -> {
                lyt_prev.invisible()
                txt_current.text = getString(R.string.aspek_pekerjaan_title)
                txt_next.text = getString(R.string.daftar_periksa_title)
            }
            1 -> {
                lyt_prev.visible()
                txt_prev.text = getString(R.string.aspek_pekerjaan_title)
                txt_current.text = getString(R.string.daftar_periksa_title)
                txt_next.text = getString(R.string.gas_title)
            }
            2 -> {
                lyt_prev.visible()
                txt_prev.text = getString(R.string.daftar_periksa_title)
                txt_current.text = getString(R.string.gas_title)
                txt_next.text = getString(R.string.alat_pelindung_title)
            }
            3 -> {
                lyt_prev.visible()
                txt_prev.text = getString(R.string.gas_title)
                txt_current.text = getString(R.string.alat_pelindung_title)
                txt_next.text = getString(R.string.review_permit_title)
            }
            4 -> {
                lyt_prev.visible()
                txt_prev.text = getString(R.string.alat_pelindung_title)
                txt_current.text = getString(R.string.review_permit_title)
                lyt_next.invisible()
                btn_next.text = getString(coreR.string.base_btn_save)
            }
        }
    }

    private fun askExit() {
        AlertDialog.Builder(this)
            .setTitle(this.getString(coreR.string.base_txt_warning))
            .setMessage(this.getString(R.string.aspek_pekerjaan_exit))
            .setPositiveButton(this.getString(coreR.string.base_btn_yes)) { _, _ ->
                finish()
            }
            .setNegativeButton(this.getString(coreR.string.base_btn_no)) { dialogInterface, _ -> dialogInterface.dismiss() }
            .setCancelable(true)
            .show()
    }

    private fun askSave() {
        AlertDialog.Builder(this)
            .setTitle(this.getString(coreR.string.base_txt_warning))
            .setMessage("Apakah Anda yakin dengan isian ini? Tidak dapat dirubah kembali..")
            .setPositiveButton(this.getString(coreR.string.base_btn_yes)) { _, _ ->
//                vmForm.save(true)
            }
            .setNegativeButton(this.getString(coreR.string.base_btn_no)) { dialogInterface, _ -> dialogInterface.dismiss() }
            .setCancelable(true)
            .show()
    }
}