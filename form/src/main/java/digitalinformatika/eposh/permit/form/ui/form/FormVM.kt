package digitalinformatika.eposh.permit.form.ui.form

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.galee.core.BaseVM
import digitalinformatika.eposh.permit.form.data.remote.FormRepository

sealed class FormState
object DefaultState : FormState()
data class Step1(val jsonKategori: String) : FormState() // ASPEK PEKERJAAN
data class Step2(val jsonKategori: String, val jsonAspek: String) : FormState() // DAFTAR PERIKSA
data class Step3(val jsonKategori: String, val jsonAspek: String, val jsonPeriksa: String) :
    FormState() // PEMERIKSAAN GAS

data class Step4(
    val jsonKategori: String, val jsonAspek: String, val jsonPeriksa: String, val jsonGas: String
) : FormState() // ALAT PELINDUNG

data class Step5(
    val jsonKategori: String, val jsonAspek: String, val jsonPeriksa: String, val jsonGas: String,
    val jsonPelindung: String
) : FormState() // REVIEW FORM

class FormVM(context: Context, private val repo: FormRepository) : BaseVM(context) {

    override fun getTagName(): String = javaClass.simpleName

    val isReview = MutableLiveData<Boolean>()
    val formState = MutableLiveData<FormState>()
    val jsonKategori = MutableLiveData<String>()
    val jsonAspek = MutableLiveData<String>()
    val jsonPeriksa = MutableLiveData<String>()
    val jsonGas = MutableLiveData<String>()
    val jsonPelindung = MutableLiveData<String>()

    init {
        isReview.value = false
        formState.value = DefaultState
        jsonKategori.value = ""
        jsonAspek.value = ""
        jsonPeriksa.value = null
        jsonGas.value = null
        jsonPelindung.value = null
    }


}