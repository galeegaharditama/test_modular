package digitalinformatika.eposh.permit.form.ui.form.step1

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.galee.core.BaseVM
import com.galee.core.R as coreR
import com.galee.core.data.model.ValidationModel
import com.galee.domain.BaseResponse
import com.galee.domain.domain.AreaDomain
import com.galee.domain.domain.AspekPekerjaanDomain
import com.galee.domain.remote.internal.util.NetworkUtil
import digitalinformatika.eposh.permit.form.R
import digitalinformatika.eposh.permit.form.data.remote.FormRepository
import kotlinx.coroutines.launch
import timber.log.Timber

class AspekPekerjaanVM(context: Context, private val repo: FormRepository) : BaseVM(context) {

    override fun getTagName(): String = javaClass.simpleName

    val uiState = MutableLiveData<BaseResponse<String>>()
    val areaState = MutableLiveData<MutableList<AreaDomain>>()
    val aspekDomain = MutableLiveData<AspekPekerjaanDomain>()

    fun setAspekPekerjaan(jsonAspek: String){
        viewModelScope.launch {
            kotlin.runCatching {
                repo.setAspekPekerjaan(jsonAspek)
            }.onSuccess {
                aspekDomain.postValue(it)
            }.onFailure { throwable ->
                Timber.e(throwable.localizedMessage)
                uiState.postValue(BaseResponse.error(
                    message = mContext.resources.getString(coreR.string.base_error_unknown),
                    case = 2
                ))
            }
        }
    }

    fun getArea(){
        viewModelScope.launch {
            kotlin.runCatching {
                uiState.postValue(BaseResponse.loading())
                if (!NetworkUtil.isNetworkConnected(mContext)) {
                    uiState.postValue(BaseResponse.error(
                        message = mContext.resources.getString(coreR.string.base_error_no_connection),
                        case = 2
                    ))
                    return@launch
                }
                repo.getArea()
            }.onSuccess {
                areaState.postValue(it)
                uiState.postValue(BaseResponse.success("success", ""))
            }.onFailure {throwable ->
                Timber.e(throwable.localizedMessage)
                uiState.postValue(BaseResponse.error(
                    message = mContext.resources.getString(coreR.string.base_error_unknown),
                    case = 2
                ))
            }
        }
    }

    fun isValidate(aspekPekerjaan: AspekPekerjaanDomain): MutableList<ValidationModel> {
        val result: MutableList<ValidationModel> = mutableListOf()

        val validationArea = if (aspekPekerjaan.idArea == 0) {
            ValidationModel(
                false,
                mContext.getString(coreR.string.base_error_field_blank)
            )
        } else {
            ValidationModel(true, null)
        }
        validationArea.layout = R.id.aspek_form_lyt_area
        result.add(validationArea)

        val validationBagian = if (aspekPekerjaan.idUnitKerja == 0) {
            ValidationModel(
                false,
                mContext.getString(coreR.string.base_error_field_blank)
            )
        } else {
            ValidationModel(true, null)
        }
        validationBagian.layout = R.id.aspek_form_lyt_bagian
        result.add(validationBagian)
        return result
    }

}