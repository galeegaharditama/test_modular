package com.galee.modular

import android.app.Application
import android.content.Context
import android.util.Log
import com.facebook.stetho.Stetho
import com.galee.domain.BuildConfig
import com.galee.domain.Constant
import com.galee.modular.di.mainModule
import com.google.android.play.core.splitcompat.SplitCompat
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class MainApp: Application(){
    override fun onCreate() {
        super.onCreate()

        // Setting Preference URL for first time
        val settingPref = getSharedPreferences("setting_api.conf", Context.MODE_PRIVATE)
        if (!settingPref.contains("api")){
            val apiAddress = Constant.BASE_URL
            settingPref.edit().putString("api", apiAddress).apply()
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return String.format(
                        "Class:%s: Line: %s, Method: %s",
                        super.createStackElementTag(element),
                        element.lineNumber,
                        element.methodName
                    )
                }
            })
//            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@MainApp)
            modules(mainModule)
        }

        SplitCompat.install(this)
        Stetho.initializeWithDefaults(this)
    }

    open class ReleaseTree: Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }

            // log your crash to your favourite
            // Sending crash report to Firebase CrashAnalytics

            // FirebaseCrash.report(message);
            // FirebaseCrash.report(new Exception(message));
        }

    }
}