package com.galee.modular.di

import com.galee.core.coreModule
import com.galee.domain.databaseModule
import com.galee.domain.domainModule
import com.galee.domain.networkModule
import com.galee.domain.networkModuleFAN
import com.galee.modular.data.remote.AppServices
import com.galee.modular.data.repository.LoginRepository
import com.galee.modular.data.repository.LoginRepositoryImpl
import com.galee.modular.login.LoginVM
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val vmModules = module {
    viewModel { LoginVM(get(), get()) }
}

val webserviceModule = module {
    single<AppServices> { get<Retrofit>().create(AppServices::class.java) }
}

val repositoryModule = module {
    single { LoginRepositoryImpl(get(), get(), get()) as LoginRepository }
}

val mainModule = listOf(
    webserviceModule,
    repositoryModule,
    vmModules,
    domainModule,
//    databaseModule,
    networkModule,
    networkModuleFAN,
    coreModule
)