package com.galee.modular.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import com.galee.core.BaseActivity
import com.galee.core.util.hideSoftKeyboard
import com.galee.domain.BaseResponse
import com.galee.domain.Constant
import com.galee.modular.BuildConfig
import com.galee.modular.MainActivity
import com.galee.modular.R
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity() {

    companion object {
        const val TAG = "LoginActivity"
    }

    private val loginVM: LoginVM by viewModel()

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.activity_login

    override fun onCreateUI(savedInstanceState: Bundle?) {
        if (loginVM.isLogged()){
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            return
        }

        button_login_pg.setOnClickListener {
            hideSoftKeyboard()
            onRunPermission(Constant.PERMISSIONS, 1)
        }

        val rilis = when (BuildConfig.DEBUG) {
            true -> "b-"
            else -> "r-"
        }
        val version = "Version $rilis${BuildConfig.VERSION_NAME}"
        textView4.text = version

        observeVM()
    }

    private fun observeVM() {
        loginVM.loginState.observe(this, Observer { state ->
            when (state.status) {
                BaseResponse.Status.LOADING -> {
                    showDialogLoading(null)
                }
                BaseResponse.Status.ERROR -> {
                    hideDialogLoading()
                    showSnackBarMessage("danger", state.message)
                }
                BaseResponse.Status.SUCCESS -> {
                    hideDialogLoading()
                    showSnackBarMessage("success", state.message)
                    Handler().postDelayed({
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }, 1500)
                }
            }
        })

        loginVM.validationState.observe(this, Observer { validations ->
            var isValid = true
            validations.forEach {
                if (isValid && !it.status) isValid = it.status
                val layout = findViewById<View>(it.layout)
                when (layout) {
                    is TextInputLayout -> {
                        layout.isErrorEnabled = when (it.status) {
                            false -> true
                            true -> false
                        }
                        layout.error = it.message
                    }
                }
            }
        })
    }

    override fun onAllPermissionGranted(level: Int) {
        super.onAllPermissionGranted(level)
        if (level == 1) {
            loginVM.onLogin(edt_login_username.text.toString(), edt_login_password.text.toString())
        }
    }
}