package com.galee.modular.login

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.galee.core.BaseVM
import com.galee.domain.BaseResponse
import com.galee.core.data.model.ValidationModel
import com.galee.domain.domain.UserDomain
import com.galee.core.R as coreR
import com.galee.modular.data.repository.LoginRepository
import kotlinx.coroutines.launch
import timber.log.Timber

class LoginVM(context: Context, private val repo: LoginRepository) : BaseVM(context) {
    override fun getTagName(): String = javaClass.simpleName

    val loginState = MutableLiveData<BaseResponse<UserDomain>>()
    val validationState = MutableLiveData<MutableList<ValidationModel>>()

    fun isLogged(): Boolean {
        if (mProfil.idUser != -1) return true
        return false
    }

    fun onLogin(username: String, password: String) {
        val valid = repo.checkValidation(username, password)
        validationState.value = valid.validationFields
        if (!valid.isValid) {
            return
        }
        viewModelScope.launch {
            kotlin.runCatching {
                loginState.postValue(BaseResponse.loading())
                val token = mContext.getSharedPreferences("token.conf", Context.MODE_PRIVATE)
                val tokenFirebase = token.getString("token", null)
                repo.onLogin(username, password, tokenFirebase)
            }.onSuccess {
                loginState.postValue(it)
            }.onFailure { throwable ->
                Timber.e(throwable.localizedMessage)
                loginState.postValue(
                    BaseResponse.error(
                        message = mContext.resources.getString(coreR.string.base_error_unknown),
                        case = 2
                    )
                )
            }
        }
    }
}