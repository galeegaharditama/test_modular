package com.galee.modular.data.repository

import android.content.Context
import com.galee.domain.mapper.UserMapper
import com.galee.core.data.model.Validation
import com.galee.core.data.model.ValidationModel
import com.galee.domain.BaseResponse
import com.galee.domain.BuildConfig
import com.galee.domain.domain.UserDomain
import com.galee.domain.remote.internal.util.NetworkUtil
import com.galee.modular.R
import com.galee.core.R as coreR
import com.galee.modular.data.remote.AppServices

class LoginRepositoryImpl(
    private val context: Context,
    private val userMapper: UserMapper,
    private val service: AppServices
) : LoginRepository {

    override suspend fun onLogin(username: String, password: String, token: String?): BaseResponse<UserDomain> {
        if (!NetworkUtil.isNetworkConnected(context)) {
            return BaseResponse.error(
                message = context.resources.getString(coreR.string.base_error_no_connection),
                case = 2
            )
        }
        val response = service.loginAsync(username, password, token, BuildConfig.CLIENT_ID, BuildConfig.API_KEY).await()

        /*DATA DUMMY*/
        /*delay(2000)
        val dataUser = mutableListOf<User>()
        dataUser.add(User(1, "1", "Admin", "Admin", "1", "admin", "", 0, "", ""))
        dataUser.add(
            User(
                2,
                "2",
                "Kontraktor",
                "Kontraktor 1",
                "1",
                "kontraktor",
                "",
                0,
                "",
                ""
            )
        )
        dataUser.add(
            User(
                3,
                "3",
                "Approval 1",
                "Approval ke 1",
                "1",
                "approval1",
                "",
                0,
                "",
                ""
            )
        )
        dataUser.add(
            User(
                4,
                "4",
                "Approval 2",
                "Approval ke 2",
                "1",
                "approval2",
                "",
                0,
                "",
                ""
            )
        )
        dataUser.add(
            User(
                5,
                "5",
                "Approval 3",
                "Approval ke 3",
                "1",
                "approval3",
                "",
                0,
                "",
                ""
            )
        )
        dataUser.add(User(6, "6", "Guest", "Guest", "0", "guest", "", 0, "", ""))
        val response = LoginResponse("User Tidak Ditemukan", true, null)
        dataUser.forEach {
            if (username == it.username && password == it.jabatanUser) {
                response.message = "dummy"
                response.error = false
                response.data = userMapper.mapToModel(it)
            }
        }
        println("$response, $username, $password")*/
        /*END DUMMY*/
        response.data?.let {
            val editor = context.getSharedPreferences("profil.conf", Context.MODE_PRIVATE).edit()
            editor.putInt("id_user", it.idUser.toInt())
            editor.putString("id_jabatan", it.idJabatan)
            editor.putString("jabatan_user", it.jabatanUser)
            editor.putString("nama_user", it.namaUser)
            editor.putString("phone", it.phone)
            editor.putString("username", it.username)
            editor.putString("token_type", it.tokenType)
            editor.putString("access_token", it.accessToken)
            editor.putString("refresh_token", it.refreshToken)
            editor.putString("masa_berlaku", it.masaBerlaku)
            editor.apply()
            return BaseResponse.success(response.message, userMapper.mapResponseToDomain(it))
        } ?: return BaseResponse.error(context.resources.getString(coreR.string.base_error_no_data))
    }

    override fun checkValidation(username: String, password: String): Validation {
        var valid = true
        val validation: MutableList<ValidationModel> = mutableListOf()
        val validationUsername: ValidationModel
        if (username.isBlank()) {
            validationUsername = ValidationModel(false, context.getString(R.string.login_error_u_valid))
            valid = false
        } else {
            validationUsername = ValidationModel(true, null)
        }
        validationUsername.layout = R.id.lyt_login_username
        validation.add(validationUsername)

        val validationPass: ValidationModel
        if (password.isBlank()) {
            validationPass = ValidationModel(false, context.getString(R.string.login_error_p_valid))
            valid = false
        } else {
            validationPass = ValidationModel(true, null)
        }
        validationPass.layout = R.id.lyt_login_password
        validation.add(validationPass)
        return Validation(valid, validation)
    }
}