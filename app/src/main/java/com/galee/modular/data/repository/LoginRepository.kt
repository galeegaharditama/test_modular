package com.galee.modular.data.repository

import com.galee.core.data.model.Validation
import com.galee.domain.BaseResponse
import com.galee.domain.domain.UserDomain

interface LoginRepository {
    suspend fun onLogin(username: String, password: String, token: String?): BaseResponse<UserDomain>
    fun checkValidation(username: String, password: String): Validation
}