package com.galee.modular.data.remote

import com.galee.domain.remote.response.AreaResponse
import com.galee.domain.remote.response.LoginResponse
import com.galee.domain.remote.response.MasterKategoriResponse
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface AppServices {
    /**
     * API FOR LOGIN
     */
    @FormUrlEncoded
    @POST("masuk")
    fun loginAsync(
        @Field("nik") code: String,
        @Field("password") password: String,
        @Field("token_firebase") token_firebase: String?,
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String
    ): Deferred<LoginResponse>

    @GET("data/kategoriform")
    fun masterKategoriAsync(): Deferred<MasterKategoriResponse>

    @GET("data/area")
    fun masterAreaAsync(): Deferred<AreaResponse>
}