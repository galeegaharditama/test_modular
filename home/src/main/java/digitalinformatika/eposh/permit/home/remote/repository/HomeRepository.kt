package digitalinformatika.eposh.permit.home.remote.repository

import com.galee.domain.BaseResponse
import com.galee.domain.domain.KategoriDomain

interface HomeRepository {
    suspend fun getKategori(): BaseResponse<MutableList<KategoriDomain>>
}