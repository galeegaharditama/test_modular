package digitalinformatika.eposh.permit.home

import android.content.Context
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.balysv.materialripple.MaterialRippleLayout
import com.galee.core.util.invisible
import com.galee.core.util.visible
import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.galee.core.R as coreR

/*BEGIN FOR LIBRARY GROUPIE ADAPTER*/
operator fun GroupAdapter<GroupieViewHolder>.plusAssign(element: Group) = this.add(element)
operator fun GroupAdapter<GroupieViewHolder>.plusAssign(groups: Collection<Group>) = this.addAll(groups)
operator fun GroupAdapter<GroupieViewHolder>.minusAssign(element: Group) = this.remove(element)
operator fun GroupAdapter<GroupieViewHolder>.minusAssign(groups: Collection<Group>)  = this.removeAll(groups)

operator fun Section.plusAssign(element: Group)  = this.add(element)
operator fun Section.plusAssign(groups: Collection<Group>) = this.addAll(groups)
operator fun Section.minusAssign(element: Group) = this.remove(element)
operator fun Section.minusAssign(groups: Collection<Group>) = this.removeAll(groups)
/*END FOR LIBRARY GROUPIE ADAPTER*/