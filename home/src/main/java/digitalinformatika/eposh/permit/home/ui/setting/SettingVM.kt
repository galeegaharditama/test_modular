package digitalinformatika.eposh.permit.home.ui.setting

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.galee.core.BaseVM
import digitalinformatika.eposh.permit.home.remote.repository.HomeRepository

sealed class SyncState
object DefaultState : SyncState()
data class LoadingState(val msg: String? = null) : SyncState()
data class ResultState(val error: Boolean, val msg: String) : SyncState()

class SettingVM(context: Context, private val repo: HomeRepository) : BaseVM(context) {

    override fun getTagName(): String = javaClass.simpleName

    val state = MutableLiveData<SyncState>()

    init {
        state.value = DefaultState
    }
}