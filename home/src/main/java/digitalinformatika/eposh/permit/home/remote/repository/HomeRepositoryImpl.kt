package digitalinformatika.eposh.permit.home.remote.repository

import android.content.Context
import com.galee.domain.mapper.KategoriMapper
import com.galee.domain.remote.internal.util.NetworkUtil
import com.galee.domain.BaseResponse
import com.galee.domain.domain.KategoriDomain
import com.galee.modular.data.remote.AppServices
import com.galee.core.R as coreR

class HomeRepositoryImpl(
    private val context: Context,
    private val kategoriMapper: KategoriMapper,
    private val service: AppServices
) : HomeRepository {

    override suspend fun getKategori(): BaseResponse<MutableList<KategoriDomain>> {
        if (!NetworkUtil.isNetworkConnected(context)) {
            return BaseResponse.error(
                message = context.resources.getString(coreR.string.base_error_no_connection),
                case = 2
            )
        }
        val response = service.masterKategoriAsync().await()

        /*DATA DUMMY*/
        /*delay(2000)
        val dataKategori= mutableListOf<KategoriResponse>()
        dataKategori.add(KategoriResponse(1, "KategoriDomain 1", "",false))
        dataKategori.add(KategoriResponse(2, "KategoriDomain 2", "",false))
        dataKategori.add(KategoriResponse(3, "KategoriDomain 3", "",false))
        dataKategori.add(KategoriResponse(4, "KategoriDomain 4", "",false))
        dataKategori.add(KategoriResponse(5, "KategoriDomain 5", "",false))
        dataKategori.add(KategoriResponse(6, "KategoriDomain 6", "",false))
        val response =
            MasterKategoriResponse("", dataKategori.size, false, dataKategori)*/
        /*END DATA DUMMY*/

        response.data?.let {
            return BaseResponse.success(response.message, kategoriMapper.mapResponsesToListDomain(it).toMutableList())
        } ?: return BaseResponse.error(context.resources.getString(coreR.string.base_error_no_data), case = 4)
    }
}