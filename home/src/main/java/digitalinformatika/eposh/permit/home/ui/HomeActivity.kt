package digitalinformatika.eposh.permit.home.ui

import android.os.Bundle
import androidx.core.view.forEachIndexed
import androidx.lifecycle.Observer
/*import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem*/
import com.galee.core.BaseActivity
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import digitalinformatika.eposh.permit.home.R
import digitalinformatika.eposh.permit.home.ui.dashboard.DashboardFragment
import digitalinformatika.eposh.permit.home.injectFeature
import digitalinformatika.eposh.permit.home.ui.setting.SettingFragment
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : BaseActivity() {

    companion object {
        const val TAG = "HomeActivity"
    }

    //    private lateinit var navigationAdapter: AHBottomNavigationAdapter
    private val homeVM: HomeVM by viewModel()

    private var indexFragment = 0
    private val dashboardFragment = DashboardFragment.newInstance()
    /*private val permitFragment = SettingFragment.newInstance()
    private val inboxFragment = SettingFragment.newInstance()*/
    private val settingFragment = SettingFragment.newInstance()
    private var fragments = mutableListOf(dashboardFragment, null, null, settingFragment)
    private var fragmentsTag = mutableListOf(DashboardFragment.TAG, null, null, SettingFragment.TAG)

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.activity_home
    override fun onCreateUI(savedInstanceState: Bundle?) {
        injectFeature()

        /*bottom_nav.isColored = false
        bottom_nav.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
        bottom_nav.isBehaviorTranslationEnabled = false
        bottom_nav.defaultBackgroundColor = ContextCompat.getColor(this, coreR.color.grey_5)
        bottom_nav.accentColor = ContextCompat.getColor(this, coreR.color.green_400)

        navigationAdapter = AHBottomNavigationAdapter(this, R.menu.bottom_nav)
        navigationAdapter.setupWithBottomNavigation(bottom_nav)*/

        if (savedInstanceState != null) {
            homeVM.indexFragment.value = savedInstanceState.getInt("indexVM")
            bottom_nav.selectedItemId = homeVM.indexFragment.value ?: 0
            indexFragment = savedInstanceState.getInt("index")
            val transaction = supportFragmentManager.beginTransaction()
            fragments[indexFragment]?.let {
                transaction.replace(R.id.nav_main, it, fragmentsTag[indexFragment])
            }
            transaction.commit()
        }

        if (homeVM.indexFragment.value == 0){
            homeVM.indexFragment.value = R.id.dashboardFragment
            bottom_nav.selectedItemId = R.id.dashboardFragment
        }

        bottom_nav.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.dashboardFragment -> {
                    indexFragment = 0
                    if (homeVM.indexFragment.value != item.itemId){
                        homeVM.indexFragment.value = item.itemId
                        homeVM.state.value = BerandaState(true)
                        return@setOnNavigationItemSelectedListener true
                    }
                    false
                }
                R.id.menu_permit -> {
                    indexFragment = 1
                    if (homeVM.indexFragment.value != item.itemId) {
                        homeVM.indexFragment.value = item.itemId
                        homeVM.state.value = PermitState(false)
                        return@setOnNavigationItemSelectedListener true
                    }
                    false
                }
                R.id.menu_inbox -> {
                    indexFragment = 2
                    if (homeVM.indexFragment.value != item.itemId) {
                        homeVM.indexFragment.value = item.itemId
                        homeVM.state.value = InboxState(false)
                        return@setOnNavigationItemSelectedListener true
                    }
                    false
                }
                R.id.settingFragment -> {
                    indexFragment = 3
                    if (homeVM.indexFragment.value != item.itemId) {
                        homeVM.indexFragment.value = item.itemId
                        homeVM.state.value = SettingState(false)
                        return@setOnNavigationItemSelectedListener true
                    }
                    false
                }
                else -> false
            }
        }
        /*bottom_nav.currentItem = indexFragment
        bottom_nav.setOnTabSelectedListener { position, wasSelected ->
            if (!wasSelected && bottom_nav.getItem(position).getTitle(this) == getString(R.string.menu_beranda)) {
                homeVM.state.value = BerandaState(true)
                homeVM.indexFragment.value = 0
                true
            } else if (!wasSelected && bottom_nav.getItem(position).getTitle(this) == getString(R.string.menu_permit)) {
                homeVM.state.value = PermitState(false)
                homeVM.indexFragment.value = 1
                true
            } else if (!wasSelected && bottom_nav.getItem(position).getTitle(this) == getString(R.string.menu_pesan)) {
                homeVM.state.value = InboxState(false)
                homeVM.indexFragment.value = 2
                true
            } else if (!wasSelected && bottom_nav.getItem(position).getTitle(this) == getString(R.string.menu_pengaturan)) {
                homeVM.state.value = SettingState(false)
                homeVM.indexFragment.value = 3
                true
            } else false
        }*/
        initVM()
        initView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        homeVM.indexFragment.value?.let { outState.putInt("indexVM", it) }
        outState.putInt("index", indexFragment)
        super.onSaveInstanceState(outState)
    }

    private fun initView() {
        /*if (profil.idJabatan == "0") {
            bottom_nav.removeItemAtIndex(1)
        }*/
    }

    private fun initVM() {
        homeVM.indexFragment.observe(this, Observer { index ->
            index?.let {
                val transaction = supportFragmentManager.beginTransaction()
                bottom_nav.menu.forEachIndexed { i, item ->
                    val fragment = supportFragmentManager.findFragmentByTag(fragmentsTag[i])
                    if (item.itemId == index){
                        fragments[i]?.let {
                            if (fragment == null) {
                                transaction.add(R.id.nav_main, it, fragmentsTag[i])
                            }
                            transaction.show(it)
                        }
                    } else {
                        if (fragment != null) {
                            fragments[i]?.let {
                                transaction.hide(it)
                            }
                        }
                    }
                }
                /*indexFragment = index
                for (i in 0 until fragments.size) {
                    val fragment = supportFragmentManager.findFragmentByTag(fragmentsTag[i])
                    if (i == index) {
                        fragments[i]?.let {
                            if (fragment == null) {
                                transaction.add(R.id.nav_main, it, fragmentsTag[i])
                            }
                            transaction.show(it)
                        }
                    } else {
                        if (fragment != null) {
                            fragments[i]?.let {
                                transaction.hide(it)
                            }
                        }
                    }
                }*/
                transaction.commit()
            }
        })
    }

    override fun onBackPressed() {
        val fragmentList = supportFragmentManager.fragments
        val contains = fragmentList[fragmentList.size - 1].javaClass.simpleName.toLowerCase() in arrayOf(
            "fragmentlistapprove"
        )
        if (contains)
            supportFragmentManager.popBackStack()
        else {
            doExitApp()
            /*val words = temp.split("-")
            if (words.size <= 2)
                if (bottom_nav.currentItem != 0) {
                    temp = ""
                    bottom_nav.currentItem = 0
                } else doExitApp()
            else {
                temp = temp.replace("-" + words[words.size - 1], "")
                var pos = bottom_nav.currentItem
                if (bottom_nav.currentItem == pos)
                    pos = 2
                else
                    pos = 1
                bottom_nav.currentItem = words[words.size - pos].toInt()
            }*/
        }
    }

    private var exitTime: Long = 0
    private fun doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            showToastMessage("info", "Tekan sekali lagi untuk keluar")
            exitTime = System.currentTimeMillis()
        } else {
            super.onBackPressed()
        }
    }
}