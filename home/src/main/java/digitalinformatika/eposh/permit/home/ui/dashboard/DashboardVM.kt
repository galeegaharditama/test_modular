package digitalinformatika.eposh.permit.home.ui.dashboard

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.galee.core.BaseVM
import com.galee.domain.BaseResponse
import com.galee.domain.domain.KategoriDomain
import digitalinformatika.eposh.permit.home.remote.repository.HomeRepository
import kotlinx.coroutines.launch
import timber.log.Timber
import com.galee.core.R as coreR

class DashboardVM(context: Context, private val repo: HomeRepository) : BaseVM(context) {

    override fun getTagName(): String = javaClass.simpleName

    val kategoriState = MutableLiveData<BaseResponse<MutableList<KategoriDomain>>>()

    fun getKategoriAPI() {
        viewModelScope.launch {
            kotlin.runCatching {
                kategoriState.postValue(BaseResponse.loading())
                repo.getKategori()
            }.onSuccess {
                kategoriState.postValue(it)
            }.onFailure {
                Timber.e(it.localizedMessage)
                kategoriState.postValue(BaseResponse.error(
                    message = mContext.resources.getString(coreR.string.base_error_unknown),
                    case = 2
                ))
            }
        }
    }
}