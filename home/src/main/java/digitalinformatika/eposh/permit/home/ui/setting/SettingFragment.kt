package digitalinformatika.eposh.permit.home.ui.setting

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.galee.core.BaseFragment
import com.galee.domain.Constant
import com.galee.domain.domain.UserDomain
import com.galee.modular.login.LoginActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import digitalinformatika.eposh.permit.home.R
import digitalinformatika.eposh.permit.home.plusAssign
import digitalinformatika.eposh.permit.home.ui.setting.item.*
import kotlinx.android.synthetic.main.fragment_setting.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import com.galee.core.R as coreR

class SettingFragment : BaseFragment(), ProfileItem.ListenerProfil, SettingButton.ListenerButton,
    SettingButtonLabel.ListenerButton {

    companion object {
        const val TAG = "SettingFragment"
        fun newInstance() = SettingFragment()
    }

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.fragment_setting

    private val profil by lazy {
        mView.context.getSharedPreferences("profil.conf", Context.MODE_PRIVATE)
    }
    private val setting by lazy {
        mView.context.getSharedPreferences("setting_api.conf", Context.MODE_PRIVATE)
    }
    private val groupAdapter = GroupAdapter<GroupieViewHolder>()
    private var flagPermission = 0
    private val vm: SettingVM by viewModel()

    override fun onClickButton(view: View, position: Int, label: String, id: Int) {
        when(id){
            -1 -> {
                profil.edit().clear().apply()

                activity?.startActivity(Intent(mView.context, LoginActivity::class.java))
                activity?.finish()
            }
        }
    }

    override fun onCreateUI(view: View, savedInstanceState: Bundle?) {
        view.toolbar.title = getString(R.string.menu_pengaturan)

        view.recycler.apply {
            hasFixedSize()
            val linearLayout = LinearLayoutManager(mView.context)
            layoutManager = linearLayout
            adapter = groupAdapter
        }

        initAdapter()
        initVM()
    }

    override fun onProfileClick(view: View, position: Int, obj: UserDomain) {
        showSnackBarMessage(obj.namaUser)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK){
            initAdapter()
        }
    }

    private fun initVM(){
        vm.state.observe(this, Observer { state ->
            when (state) {
                is DefaultState -> {
                }

                is LoadingState -> {
                    showDialogLoading(state.msg)
                }

                is ResultState -> {
                    hideDialogLoading()
                    showSnackBarMessage(state.msg)
                }
            }
        })

    }

    private fun initAdapter(){
        groupAdapter.clear()

        groupAdapter += Section(SettingSeparate()).apply {
            add(ProfileItem(vm.mProfil, this@SettingFragment))
        }

        groupAdapter += Section(SettingSeparate()).apply {
            add(SettingButton(-1,true, null, getString(coreR.string.base_btn_exit), this@SettingFragment))
        }
    }
}