package digitalinformatika.eposh.permit.home.ui.dashboard

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.galee.core.BaseFragment
import com.galee.core.util.CoreUtil
import com.galee.core.util.Router
import com.galee.core.util.gone
import com.galee.core.util.visible
import com.galee.core.util.widget.ErrorView
import com.galee.domain.BaseResponse
import com.galee.domain.Constant
import com.galee.domain.domain.KategoriDomain
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import digitalinformatika.eposh.permit.home.ui.BerandaState
import digitalinformatika.eposh.permit.home.ui.HomeVM
import digitalinformatika.eposh.permit.home.R
import digitalinformatika.eposh.permit.home.ui.dashboard.item.ItemKategori
import kotlinx.android.synthetic.main.card_form.view.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

import com.galee.core.R as coreR

class DashboardFragment : BaseFragment() {

    companion object {
        const val TAG = "DashboardFragment"
        fun newInstance() = DashboardFragment()
    }

    private val vmMain: HomeVM by sharedViewModel()
    private val vmDashboard: DashboardVM by viewModel()
    private val mListKategori = mutableListOf<KategoriDomain>()
    private val groupAdapter = GroupAdapter<GroupieViewHolder>()
    private val sectionKategori = Section()

    override fun getTagName(): String = TAG
    override fun layoutId(): Int = R.layout.fragment_dashboard

    override fun onCreateUI(view: View, savedInstanceState: Bundle?) {
        view.toolbar.title = getString(R.string.dashboard_title)

        view.form_btn_create.setOnClickListener {
            onRunPermission(Constant.PERMISSIONS, 10)
        }

        view.form_recycler.apply {
            layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)
            adapter = groupAdapter
        }

        setupVM()
        onRunPermission(Constant.PERMISSIONS, 0)
    }

    override fun onAllPermissionGranted(level: Int) {
        super.onAllPermissionGranted(level)
        when (level) {
            0 -> vmDashboard.getKategoriAPI()
            1 -> vmDashboard.getKategoriAPI()
            10 -> {
                val selected = mutableListOf<KategoriDomain>()
                mListKategori.forEach { obj ->
                    if (obj.isSelected) selected.add(obj)
                }
                val bundle = Bundle()
                bundle.putString("CATEGORY", Gson().toJson(selected))
                bundle.putInt("SCENARIO", 1)
                Router.navigateModule(
                    this.activity as Activity,
                    "digitalinformatika.eposh.permit.form.ui.form.FormActivity",
                    bundle,
                    false
                )
            }
        }
    }

    override fun onDenyPermission(level: Int) {
        super.onDenyPermission(level)
        if (level == 0) {
            isErrorResponse(0, null, 1)
        }
    }

    private fun setupVM() {
        vmDashboard.kategoriState.observe(this, Observer { state ->
            when (state.status) {
                BaseResponse.Status.LOADING -> {
                    mView.form_progress?.visible()
                    mView.form_layout?.gone()
                    mView.form_lyt_error?.gone()
                }
                BaseResponse.Status.SUCCESS -> {
                    mListKategori.clear()
                    groupAdapter.clear()
                    sectionKategori.clear()

                    mView.form_progress?.gone()
                    mView.form_layout?.visible()
                    state.data?.let { mListKategori.addAll(it) }
                    state.data?.mapIndexed { index, kategori ->
                        sectionKategori.add(ItemKategori(kategori) { item, selected ->
                            mListKategori[index].isSelected = selected
                            item.setSelected(selected)
                            item.notifyChanged("SELECTED")
                            isEnabledButtonForm()
                        })
                    }
                    groupAdapter.add(sectionKategori)
                    isEnabledButtonForm()
                }
                BaseResponse.Status.ERROR -> {
                    mView.form_progress?.gone()
                    isErrorResponse(state.case ?: 2, state.message, 1)
                }
            }
        })

        vmMain.state.observe(this, Observer { state ->
            when (state) {
                is BerandaState -> {
                    if (state.isRefresh) onRunPermission(Constant.PERMISSIONS, 0)
                }
            }
        })
    }

    private fun isErrorResponse(case: Int, msg: String?, type: Int) {
        when (type) {
            1 -> { // LAYOUT KATEGORI
                mView.form_lyt_error?.setView(true, msg, case, object : ErrorView.ErrorListener {
                    override fun onReloadData() {
                        onRunPermission(Constant.PERMISSIONS, 1)
                    }
                })
                mView.form_lyt_error?.visible()
                mView.form_layout?.gone()
            }
        }
    }

    private fun isEnabledButtonForm() {
        var anySelected = false
        mListKategori.forEach {
            if (it.isSelected) anySelected = true
        }
        CoreUtil.isEnabledClick(
            mView.context,
            anySelected,
            mView.form_btn_create,
            coreR.color.green_400,
            coreR.color.grey_400
        )
    }
}