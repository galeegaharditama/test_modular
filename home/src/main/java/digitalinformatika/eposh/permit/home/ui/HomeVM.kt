package digitalinformatika.eposh.permit.home.ui

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.galee.core.BaseVM

sealed class MainState
object DefaultState : MainState()
data class BerandaState(val isRefresh:Boolean) : MainState()
data class PermitState(val isRefresh:Boolean) : MainState()
data class InboxState(val isRefresh:Boolean) : MainState()
data class SettingState(val isRefresh:Boolean) : MainState()
object LoadingState : MainState()

class HomeVM(context: Context) : BaseVM(context) {

    override fun getTagName(): String = javaClass.simpleName

    val state = MutableLiveData<MainState>()
    val indexFragment = MutableLiveData<Int>()

    init {
        state.value = DefaultState
        indexFragment.value = 0
    }
}