package digitalinformatika.eposh.permit.home.ui.setting.item

import android.view.View
import androidx.core.content.ContextCompat
import com.galee.core.util.gone
import com.galee.core.util.invisible
import com.galee.domain.domain.UserDomain
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import digitalinformatika.eposh.permit.home.R
import com.galee.core.R as coreR
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.item_setting_button.view.*
import kotlinx.android.synthetic.main.item_setting_button_with_sublabel.view.*
import kotlinx.android.synthetic.main.item_setting_profile.view.*

open class SettingItem(private val title: String) : Item() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.title_section.text = title
    }

    override fun getLayout(): Int = R.layout.item_header
}

class SettingButton(
    private val id: Int,
    private val isLogout: Boolean,
    private val image: Int?,
    private val label: String?,
    private val listener: ListenerButton
) : Item() {
    interface ListenerButton {
        fun onClickButton(view: View, position: Int, label: String, id: Int)
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        image?.let {
            viewHolder.itemView.image.setImageResource(image)
        } ?: viewHolder.itemView.image.invisible()

        label?.let {
            if (isLogout)
                viewHolder.itemView.txt_label.setTextColor(
                    ContextCompat.getColor(
                    viewHolder.itemView.context,
                    coreR.color.red_400)
                )
            viewHolder.itemView.txt_label.text = label
        } ?: viewHolder.itemView.txt_label.gone()

        viewHolder.itemView.lyt_btn.setOnClickListener {
            listener.onClickButton(it, position, label ?: "-", id)
        }
    }

    override fun getLayout(): Int = R.layout.item_setting_button
}

class SettingButtonLabel(
    private val id: Int,
    private val image: Int?,
    private val label: String,
    private val subLabel: String,
    private val listener: ListenerButton
) : Item() {
    interface ListenerButton {
        fun onClickButton(view: View, position: Int, label: String, id: Int)
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        image?.let {
            viewHolder.itemView.sub_image.setImageResource(image)
        } ?: viewHolder.itemView.sub_image.invisible()

        viewHolder.itemView.sub_txt_label.text = label
        viewHolder.itemView.sub_txt_sub_label.text = subLabel

        viewHolder.itemView.sub_lyt_btn.setOnClickListener {
            listener.onClickButton(it, position, label, id)
        }
    }

    override fun getLayout(): Int = R.layout.item_setting_button_with_sublabel
}

class ProfileItem(val profil: UserDomain, private val listener: ListenerProfil) : Item() {
    interface ListenerProfil {
        fun onProfileClick(view: View, position: Int, obj: UserDomain)
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.txt_name_user.text = profil.namaUser
        viewHolder.itemView.txt_jabatan_user.text = profil.jabatanUser
        viewHolder.itemView.txt_no_telp.text = profil.phone

        viewHolder.itemView.txt_edit_user.setOnClickListener {
            listener.onProfileClick(it, position, profil)
        }
    }

    override fun getLayout(): Int = R.layout.item_setting_profile
}

class SettingSeparate : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {}
    override fun getLayout(): Int = R.layout.item_setting_separate
}