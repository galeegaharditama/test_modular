package digitalinformatika.eposh.permit.home

import com.galee.modular.data.remote.AppServices
import digitalinformatika.eposh.permit.home.ui.dashboard.DashboardVM
import digitalinformatika.eposh.permit.home.remote.repository.HomeRepository
import digitalinformatika.eposh.permit.home.remote.repository.HomeRepositoryImpl
import digitalinformatika.eposh.permit.home.ui.HomeVM
import digitalinformatika.eposh.permit.home.ui.setting.SettingVM
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

fun injectFeature() = loadFeature

private val loadFeature by lazy {
    loadKoinModules(
        arrayListOf(homeModule, vmModule)
    )
}

val homeModule = module {
    single { HomeRepositoryImpl(get(), get(), get()) as HomeRepository }
//    single<AppServices> { get<Retrofit>().create(AppServices::class.java) }
}

val vmModule = module{
    viewModel { HomeVM(get()) }
    viewModel { DashboardVM(get(), get()) }
    viewModel { SettingVM(get(), get()) }
}