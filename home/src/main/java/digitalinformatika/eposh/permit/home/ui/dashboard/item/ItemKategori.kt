package digitalinformatika.eposh.permit.home.ui.dashboard.item

import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.galee.domain.domain.KategoriDomain
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import digitalinformatika.eposh.permit.home.R
import kotlinx.android.synthetic.main.item_kategori.view.*

class ItemKategori(
    private val obj: KategoriDomain,
    private val onKategoriListener: (item: ItemKategori, selected: Boolean) -> Unit
) : Item() {

    private var isSelected = false

    fun setSelected(selected: Boolean) {
        isSelected = selected
    }

    private fun bindSelected(holder: GroupieViewHolder) {
        holder.itemView.card_category.isChecked = isSelected
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.title.text = obj.title
        Glide.with(viewHolder.itemView.context)
            .load(obj.logo)
            .into(viewHolder.itemView.imageView)
        viewHolder.itemView.card_category.isChecked = isSelected
        viewHolder.itemView.card_category.setOnClickListener {
            onKategoriListener(this, !isSelected)
        }
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int, payloads: MutableList<Any>) {
        super.bind(viewHolder, position, payloads)
        if (payloads.contains("SELECTED")) {
            bindSelected(viewHolder)
        } else {
            bind(viewHolder, position)
        }
    }

    override fun getLayout(): Int = R.layout.item_kategori

    /*override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.title.text = obj.title
        Picasso.with(viewHolder.itemView.context)
            .load(obj.logo)
            .error(R.drawable.ic_image_error)
            .fit()
            .into(viewHolder.itemView.imageView)
        UtilHome.isSelectedView(obj.isSelected, viewHolder.itemView.checked)
        viewHolder.itemView.card_category.setOnClickListener { listener.onItemClick(position, obj, viewHolder.itemView.checked) }
    }*/
}