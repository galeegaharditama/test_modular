package digitalinformatika.eposh.permit.home

import androidx.lifecycle.Observer
import com.galee.core.BaseVM
import digitalinformatika.eposh.permit.home.remote.repository.HomeRepositoryImpl
import digitalinformatika.eposh.permit.home.ui.dashboard.DashboardVM
import org.junit.Before
import org.junit.Rule
import org.koin.core.context.startKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DashboardVMTest : KoinTest {
    val viewModel : DashboardVM by inject()
    val repository : HomeRepositoryImpl by inject()

    @Mock
    lateinit var uiData : Observer<BaseVM>

    @Before
    fun before(){
        MockitoAnnotations.initMocks(this)
//        startKoin(testApp)
    }
}